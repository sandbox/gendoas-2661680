<?php

/**
 * @file
 * Contains \Drupal\slogtx\TxRootTermGetManager.
 */

namespace Drupal\slogtx;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\Container;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages plugins for root term get.
 */
class TxRootTermGetManager extends DefaultPluginManager {

  /**
   * List of already instantiated plugins.
   *
   * @var array
   */
  protected $instances = [];

  /**
   *
   * @param string $type
   *   The plugin type, e.g. sys_sys, sys_node, tb_menu.
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations,
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct($type, \Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $c_type = Container::camelize($type);
    $subdir = "Plugin/slogtx/TxRootTermGet/$type";
    $plugin_definition_annotation_name = "Drupal\slogtx\Annotation\Tx{$c_type}RootTermGet";
    $plugin_interface = 'Drupal\slogtx\Plugin\slogtx\TxRootTermGetInterface';
    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);
    $this->setCacheBackend($cache_backend, "slogtx:rtget:{$type}_plugins");
  }

  /**
   * Overrides PluginManagerBase::getInstance().
   *
   * Returns the instance of the plugin for retrieving root terms.
   *
   * @param array $options
   *   An array with the following key/value pairs:
   *   - plugin_id: (string) Plugin id equals to toolbartab part of the vocabulary id
   *   - entity: (optional) EntityInterface, instead of tt_name (to build tt_name)
   *   - tt_name: (optional) Target term name, defaults to SlogTx::TARGETTERM_DEFAULT
   *   - do_create: (optional) do create target term if not exists, defaults to FALSE
   *
   * @return \Drupal\slogtx\Plugin\slogtx\TxRootTermGetInterface
   *   A TxRootTermGet plugin instance.
   */
  public function getInstance(array $options) {
    if (empty($options['plugin_id'])) {
      throw new \InvalidArgumentException('Plugin id is required');
    }

    $plugin_id = $options['plugin_id'];
    if (!empty($options['do_create']) //
            && !empty($this->instances[$plugin_id]) //
            && !$this->instances[$plugin_id]->hasTargetTerm()) {
      unset($this->instances[$plugin_id]);
    }
    
    if (empty($this->instances[$plugin_id])) {
      $configuration = ['rtget-options' => $options];
      $this->instances[$plugin_id] = $this->createInstance($plugin_id, $configuration);
    }

    return $this->instances[$plugin_id];
  }

}
