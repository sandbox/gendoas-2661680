<?php

/**
 * @file
 * Contains \Drupal\slogtx\Access\SlogToolbarAccessControlHandler.
 */

namespace Drupal\slogtx\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\slogtx\Access\SlogEntityAccessControlHandlerTrait;

/**
 * Defines the access control handler for the slogtx_tb entity.
 */
class SlogToolbarAccessControlHandler extends EntityAccessControlHandler {

  use SlogEntityAccessControlHandlerTrait;
}
