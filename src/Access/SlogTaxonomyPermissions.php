<?php

/**
 * @file
 * Contains \Drupal\slogtx\Access\SlogTaxonomyPermissions.
 */

namespace Drupal\slogtx\Access;

use Drupal\slogtx\SlogTx;
use Drupal\taxonomy\TaxonomyPermissions;

/**
 * Provides dynamic permissions of the slogtx module.
 *
 * @see slogtx.permissions.yml
 */
class SlogTaxonomyPermissions extends TaxonomyPermissions {

  /**
   * Get slog taxonomy permissions.
   *
   * @return array
   *   Permissions array.
   */
  public function permissions() {
    $permissions = [
      "administer slog taxonomy" => ['title' => $this->t('Administer all toolbars')],
    ];
    // toolbars: administer only
    $toolbars = SlogTx::getToolbarsSorted(TRUE);
    foreach ($toolbars as $toolbar) {
      if (!$toolbar->isUnderscoreToolbar()) {
        $toolbar_id = $toolbar->id();
        $disabled = $toolbar->status() ? '' : ' - disabled';
        $args = ['%toolbar_id' => $toolbar->id(), '%toolbar' => $toolbar->label(), '%disabled' => $disabled];
        $permissions += [
          "administer toolbar $toolbar_id" => ['title' => $this->t('Administer toolbar %toolbar_id (%toolbar)%disabled', $args)],
        ];
      }
    }
    return $permissions;
  }

}
