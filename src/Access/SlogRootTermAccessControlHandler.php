<?php

/**
 * @file
 * Contains \Drupal\slogtx\Access\SlogRootTermAccessControlHandler.
 */

namespace Drupal\slogtx\Access;

use Drupal\taxonomy\TermAccessControlHandler;
use Drupal\slogtx\Access\SlogEntityAccessControlHandlerTrait;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the slogtx_rt entity.
 */
class SlogRootTermAccessControlHandler extends TermAccessControlHandler {

  use SlogEntityAccessControlHandlerTrait {
    checkAccess as traitCheckAccess;
  }

  /**
   * {@inheritdoc}
   */
  public function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    // handle 'disable', parent doesn't.
    if ($operation == 'disable') {
      return ($entity->isLocked('disable')) ? AccessResult::forbidden() : AccessResult::allowed();
    }

    return $this->traitCheckAccess($entity, $operation, $account);
  }

}
