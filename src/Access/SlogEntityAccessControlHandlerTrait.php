<?php

/**
 * @file
 * Contains \Drupal\slogtx\Access\SlogEntityAccessControlHandlerTrait.
 */

namespace Drupal\slogtx\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

trait SlogEntityAccessControlHandlerTrait {

  /**
   * {@inheritdoc}
   */
  public function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $operations = ['delete', 'update', 'disable', 'reorder',];
    if (in_array($operation, $operations) && $entity->isLocked($operation)) {
      // override default
      return AccessResult::forbidden();
    }

    // default
    return parent::checkAccess($entity, $operation, $account);
  }

}
