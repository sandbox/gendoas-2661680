<?php

/**
 * @file
 * Contains \Drupal\slogtx\Access\SlogTargetEntityAccessControlHandler.
 */

namespace Drupal\slogtx\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\slogtx\Access\SlogEntityAccessControlHandlerTrait;

/**
 * Defines the access control handler for the slogtx_voc entity.
 */
class SlogTargetEntityAccessControlHandler extends EntityAccessControlHandler {

  use SlogEntityAccessControlHandlerTrait;
}
