<?php

/**
 * @file
 * Contains \Drupal\slogtx\Access\SlogVocabularyAccessControlHandler.
 */

namespace Drupal\slogtx\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\slogtx\Access\SlogEntityAccessControlHandlerTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the slogtx_voc entity.
 */
class SlogVocabularyAccessControlHandler extends EntityAccessControlHandler {

  use SlogEntityAccessControlHandlerTrait {
    checkAccess as traitCheckAccess;
  }
  /**
   * {@inheritdoc}
   */
  public function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
   return $this->traitCheckAccess($entity, $operation, $account);
  }

}
