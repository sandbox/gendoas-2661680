<?php

/**
 * @file
 * Contains \Drupal\slogtx\Plugin\slogtx\TxSysNodeRootTermGet.
 */

namespace Drupal\slogtx\Plugin\slogtx;

use Drupal\slogtx\SlogTx;

/**
 * Defines the abstract class for plugins on the _node toolbar.
 */
abstract class TxSysNodeRootTermGet extends TxRootTermGetBase {
  
  protected function _getBaseToolbarID() {
    return SlogTx::TOOLBAR_ID_NODE_SUBSYS;
  }
  
}
