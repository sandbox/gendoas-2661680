<?php

/**
 * @file
 * Contains \Drupal\slogtx\Plugin\slogtx\TxTbMenuRootTermGet.
 */

namespace Drupal\slogtx\Plugin\slogtx;

/**
 * Defines the abstract class for plugins on menu toolbars.
 */
abstract class TxTbMenuRootTermGet extends TxRootTermGetBase {
  
  protected function _getBaseToolbarID() {
    return $this->get('toolbar_id');
  }
  
  protected function _isValidToolbar() {
    return $this->toolbar->isMenuToolbar();
  }
  
}
