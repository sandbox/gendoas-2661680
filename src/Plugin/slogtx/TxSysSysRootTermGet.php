<?php

/**
 * @file
 * Contains \Drupal\slogtx\Plugin\slogtx\TxSysSysRootTermGet.
 */

namespace Drupal\slogtx\Plugin\slogtx;

use Drupal\slogtx\SlogTx;

/**
 * Defines the abstract class for plugins on the _sys toolbar.
 */
abstract class TxSysSysRootTermGet extends TxRootTermGetBase {
  
  protected function _getBaseToolbarID() {
    return SlogTx::TOOLBAR_ID_SYS;
  }
  
}
