<?php

/**
 * @file
 * Contains \Drupal\slogtx\Plugin\slogtx\TargetEntityBase.
 */

namespace Drupal\slogtx\Plugin\slogtx;

use Drupal\slogtx\SlogTx;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the base class for target entity plugins.
 */
class TargetEntityBase extends PluginBase implements TargetEntityInterface {

  protected $vocabulary;
  protected $entity;

  /**
   * The name of the current target term.
   * 
   * - Target term is a term without a parent term
   * - Root term is the term with only the target term as parent term
   * - There may be multiple target terms with multiple root terms each
   * - Menu items are built by the children of root terms
   * - The vocabulary serves term trees for exactly one root term at once
   * - For example for node target entity the target term's name may be Node.123,
   *   or for role target entity the target term's name may be UserRole.administrator
   * - For target entity == 'none' the default target term name is 'Default'
   * 
   * The current target term name is stored as state in key_value table.
   * 
   * @var string
   */
  protected $currentTargetTermName;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // Vocabulary is not always needed, e.g. for options list
    $no_vocabulary = isset($configuration['no_vocabulary']) ? TRUE : FALSE;
    $this->vocabulary = isset($configuration['vocabulary']) ? $configuration['vocabulary'] : NULL;
    if (!$no_vocabulary && !isset($this->vocabulary)) {
      $message = t('Plugin instance built without vocabulary: %id.', ['%id' => $this->getPluginId()]);
      throw new \LogicException($message);
    }
    // 
    $this->setDefaultEntity();
  }

  /**
   * {@inheritdoc}
   */
  public function saveSettings(array $settings) {
    if (is_array($settings)) {
      SlogTx::pluginManager('target_entity') //
          ->setSettings($settings, $this->getPluginId());
      $this->configuration = array_merge($this->configuration, $settings);
    }

    return $this;
  }

  /**
   * Whether this target entity plugin is a valid one.
   */
  public function isValid() {
    return TRUE;
  }

  /**
   * Whether this object woks without an entity.
   * 
   * @return boolean
   */
  public function isNoneEntityType() {
    return ($this->getPluginId() === SlogTx::TARGETENTITY_ID_NONE);
  }

  /**
   * {@inheritdoc}
   */
  public function isNodeEntityType() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultTargetTermName() {
    if (!$entity = $this->getCurrentEntity()) {
      if (!$entity = $this->setDefaultEntity()) {
        $args = [
          '@voc' => $this->vocabulary->id(),
          '@pid' => $this->getPluginId(),
        ];
        $message = t("Couldn't set default target term from entity: (@eid/@voc).", $args);
        throw new \LogicException($message);
      }
    }
    return $this->buildTargetTermName($entity->id());
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentTargetTermName() {
    if (empty($this->currentTargetTermName)) {
      $this->currentTargetTermName = $this->getDefaultTargetTermName();
    }
    return $this->currentTargetTermName;
  }

  /**
   * 
   * @param type $term_name
   * @return string
   */
  public function setCurrentTargetTermName(string $term_name) {
    if ($term_name === SlogTx::TARGETTERM_DEFAULT) {
      $this->currentTargetTermName = $term_name;
    }
    elseif ($entity = $this->getEntityFromTargetTermName($term_name)) {
      $this->setEntity($entity);
    }
    else {
      $this->currentTargetTermName = NULL;
    }

    return $this->getCurrentTargetTermName();
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetTermPrefix() {
    if (empty($this->target_term_prefix)) {
      $words = str_replace('_', ' ', $this->getPluginId());
      $this->target_term_prefix = str_replace(' ', '', ucwords($words));
    }
    return $this->target_term_prefix;
  }

  /**
   * Whether the target term exists.
   */
  protected function targetTermExists($target_term_id, &$err_msg) {
    // does target term exist
    $target_term = $this->vocabulary->getTargetTerm($target_term_id);
    if (!$target_term) {
      $err_msg = t('Target term %term not found.', ['%term' => $target_term_id]);
      return FALSE;
    }

    // OK
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isValidTargetTermId($target_term_id, &$err_msg) {
    if (!$this->targetTermExists($target_term_id, $err_msg)) {
      return FALSE;
    }

    // test for entity
    $target_term = $this->vocabulary->getTargetTerm($target_term_id);
    $entity_type_id = $this->getPluginId();
    $entity_id = $this->getTargetEntityId($target_term->label());
    if ($this->isValidEntityId($entity_id)) {
      $entity = SlogTx::entityInstance($entity_type_id, $entity_id);
    }
    if (!$entity || $entity->getEntityTypeId() != $entity_type_id) {
      $args = [
        '%entity' => $entity_id,
        '%type' => $entity_type_id,
      ];
      $err_msg = t('Entity %entity of type %type not found or not valid.', $args);
      return FALSE;
    }

    // OK
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildTargetTermName($entity_id = 0) {
    if (!$this->entityExists($entity_id)) {
      return SlogTx::TARGETTERM_DEFAULT;
    }

    return $this->getTargetTermPrefix() . '.' . $entity_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityId($term_name) {
    $prefix = $this->getTargetTermPrefix();
    $parts = explode('.', $term_name);
    return (count($parts) === 2 && $parts[0] == $prefix) ? $parts[1] : NULL;
  }

  /**
   * Return an entity which belongs to the target term name.
   * 
   * @param string $term_name 
   *  The target term name, e.g. User.1, Role.authenticated
   * @return EntityInterface on success, otherwise FALSE
   */
  public function getEntityFromTargetTermName(string $term_name) {
    $is_default = ($term_name === SlogTx::TARGETTERM_DEFAULT);
    if (!$is_default && !$this->isNoneEntityType()) {
      $entity_type_id = $this->getPluginId();
      $entity_id = $this->getTargetEntityId($term_name);
      if ($this->isValidEntityId($entity_id)) {
        $entity = SlogTx::entityInstance($entity_type_id, $entity_id) ?: NULL;
      }

      if (!$entity) {
        $args = [
          '@vid' => $this->vocabulary->id(),
          '@name' => $term_name,
        ];
        $msg = t('Entity from target term name not found (@vid/@name).', $args);
        SlogTx::logger()->error($msg);
      }
    }

    return ($entity ?? FALSE);
  }

  /**
   * Return the class for editing TbTab data (overrides - label, description)
   * 
   * @return string
   */
  public function getTbtabEditClass() {
    return $this->tbtab_edit_class;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredTargetTerms() {
    $required = [];
    if ($this->isValid()) {
      $required[SlogTx::TARGETTERM_DEFAULT] = [
        'description' => t('The Default target term: is required, not deletable, not to disable and name not changeable.'),
        'locked' => ['delete', 'disable'],
      ];
    }

    return $required;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->configuration['title'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->configuration['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityRequiredHint($pre = '', $post = '') {
    $hint = $this->entity_required_hint;
    if (empty($hint)) {
      $hint = SlogTx::defaultEntityRequiredHint();
    }
    return ($pre . $hint . $post);
  }

  /**
   * {@inheritdoc}
   */
  public function getValues() {
    return $this->configuration;
  }

  /**
   * Return the current entity object,
   * 
   * The current entity object has to be set by setEntity().
   * 
   * @return entity object or NULL
   *  If object then of type equals to my own id
   */
  public function getCurrentEntity() {
    return $this->entity;
  }

  /**
   * Set the current entity object.
   * 
   * Configurations are dependent of this entity.
   * 
   * @param EntityInterface $entity
   * @return EntityInterface
   * @throws \LogicException if entity's type not equal to plugin id
   */
  public function setEntity(EntityInterface $entity) {
    if ($this->validateEntityType($entity)) {
      $this->entity = $entity;
      $this->currentTargetTermName = NULL;
    }
    else {
      $args = [
        '@eid' => $entity->getEntityTypeId(),
        '@pid' => $this->getPluginId(),
      ];
      $message = t("Current entity's type not equal to plugin id: (@eid/@pid).", $args);
      throw new \LogicException($message);
    }
    return $this->getCurrentEntity();
  }

  /**
   * Function to be overridden by derived classes, except of 'none'.
   * 
   * By default set to FALSE to indicate it is already set
   * 
   * @see self::getTbtabLabel()
   * @see self::getTbtabDescription()
   * 
   * @return EntityInterface
   */
  public function setDefaultEntity() {
    $this->entity = FALSE;
    return $this->getCurrentEntity();
  }

  /**
   * Return the config key dependent on vocabulary and current entity.
   * 
   * @return string composed config key
   *  This is composed of 'xt_tbtab.', vocabulary id and entity id.
   * @throws \LogicException if there is no entity
   */
  public function getConfigKey() {
    if (!empty($this->entity)) {
      $provider = $this->provider;
      $vid = $this->vocabulary->id();
      $target_name = $this->buildTargetTermName($this->entity->id());
      // ensure provider as first part
      return "$provider.$vid.$target_name";
    }
    else {
      $message = t("Entity not set for @pid.", ['@pid' => $this->getPluginId()]);
      throw new \LogicException($message);
    }
  }

  /**
   * Return the config object of the locally generated key.
   * 
   * @return Drupal\Core\Config\Config
   */
  public function getTbtabConfig() {
    return \Drupal::config($this->getConfigKey());
  }

  /**
   * return an user friendly label for type + current entity label. 
   * 
   * @return string
   */
  public function getOptionLabel($extend = TRUE) {
    $do_extend = ($extend && $this->entity_required);
    $prefix = $this->getTitle();
    $target = $do_extend ? '?' : t('Global');
    if ($entity = $this->getCurrentEntity()) {
      $target = $entity->label();
    }
    elseif ($do_extend) {
      $target .= $this->getEntityRequiredHint(' - ');
    }
    return "$prefix.$target";
  }

  public function getSystbLabel() {
    //todo::current::from vocabs build 
    $vid = $this->vocabulary->id();
    $toolbartab = SlogTx::getToolbartabFromVid($vid);
    $parts = explode('_', $toolbartab, 2);
    if (count($parts) === 2 && $parts[1] === $this->getPluginId()) {
      return ucfirst($parts[0]);
    }
    return $this->vocabulary->label();
  }

  /**
   * Return the entity specific toolbar tab label.
   * 
   * @param string $default to return if label is not overriden.
   * @return string
   */
  public function getTbtabLabel($add_entity_label = FALSE) {
    $vocab = $this->vocabulary;
    $vocab_label = $vocab->label();
    if (!$this->isEntityRequired()) {
      return $vocab_label;
    }

    if (empty($this->entity)) {
      return "$vocab_label.???";
    }

    $tbtab_label = $this->getTbtabConfig()->get('name');
    $entity_label = $this->entity->label();
    if (empty($tbtab_label)) {
      $add_entity_label = FALSE;
      $entity_id = $this->entity->id();
      $te_title = $this->getTitle();
      if ($vocab->isUnderscoreToolbar()) {
        $tbtab_label = "$te_title.$entity_label";
        if ($vocab->isSysToolbar() && $sys_label = $this->getSystbLabel()) {
          $tbtab_label = $sys_label . '.' . $tbtab_label;
        }
      }
      else {
        $tbtab_label = "$entity_label::$vocab_label";
      }
    }

    if ($add_entity_label) {
      $tbtab_label = "$entity_label::$tbtab_label";
    }

    return $tbtab_label;
  }

  /**
   * Return the entity specific toolbar tab description.
   * 
   * @param string $default to return if description is not overriden.
   * @return string
   */
  public function getTbtabDescription($default = '???description???') {
    if (!empty($this->entity)) {
      $pre = $this->entity->label() . ': ';
      $descr = $this->getTbtabConfig()->get('description');
    }
    return (!empty($descr) ? $pre . $descr : $default);
  }

  /**
   * Ensure entity's type id equals this plugin id.
   * 
   * @param EntityInterface $entity
   * @return boolean
   */
  protected function validateEntityType(EntityInterface $entity) {
    return ($entity->getEntityTypeId() == $this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function isValidEntityId($entity_id) {
    return !empty($entity_id);
  }

  public function entityExists($entity_id) {
    if (!$this->isValidEntityId($entity_id)) {
      return FALSE;
    }
    $controller = \Drupal::entityTypeManager()->getStorage($this->getPluginId());
    $entity = $controller->load($entity_id);
    return (isset($entity) && ($entity instanceof EntityInterface));
  }

  /**
   * Whether an entity is required
   * 
   * @return boolean
   */
  public function isEntityRequired() {
    return (boolean) $this->entity_required;
  }

  /**
   * Whether the plugin has a fixed entity, e.g. current user, current default user role
   * 
   * @return boolean
   */
  public function isEntityFixed() {
    return (boolean) $this->entity_is_fixed;
  }

  /**
   * Whether an entity can/must be set, e.g. node entity for _node toolbar
   * 
   * Requires: isEntityRequired() and not isEntityFixed()
   * 
   * @return boolean
   */
  public function isEntitySettable() {
    return ($this->isEntityRequired() && !$this->isEntityFixed());
  }

  /**
   * Build and return the key for the current target term.
   * 
   * The key depends on the user id and the vocabulary id.
   * 
   * @return string
   */
  protected function getStateKeyCurrentTargetTermName() {
    $uid = \Drupal::currentUser()->id();
    $vid = $this->vocabulary->id();
    return "slogtx.cur_target_term.$uid.$vid";
  }

  /**
   * {@inheritdoc}
   */
  public function __get($name) {
    if (!isset($this->configuration[$name])) {
      $this->configuration[$name] = [];
    }

    return $this->configuration[$name];
  }

  /**
   * {@inheritdoc}
   */
  public function __set($name, $value) {
    if (isset($value)) {
      $this->configuration[$name] = $value;
    }
    else {
      unset($this->configuration[$name]);
    }
  }

}
