<?php

/**
 * @file
 * Contains \Drupal\slogtx\Plugin\slogtx\TargetEntity\None.
 */

namespace Drupal\slogtx\Plugin\slogtx\TargetEntity;

use Drupal\slogtx\Plugin\slogtx\TargetEntityBase;
use Drupal\slogtx\SlogTx;

/**
 * @todo.
 *
 * @SlogtxTargetEntity(
 *   id = "none",
 *   title = @Translation("Default"),
 *   description = @Translation("Section for global subjects, not managed for entities."),
 *   tbtab_edit_class = false,
 *   entity_required = FALSE,
 *   lock_entity_required = TRUE,
 *   provider = "slogtx",
 * )
 */
class None extends TargetEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultTargetTermName() {
    return SlogTx::TARGETTERM_DEFAULT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentTargetTermName() {
    if (empty($this->currentTargetTermName)) {
      // there is no entity for plugin type 'none'
      $key = $this->getStateKeyCurrentTargetTermName();
      $term_name = \Drupal::state()->get($key, SlogTx::TARGETTERM_DEFAULT);
      if ($this->vocabulary->isValidTargetTermName($term_name)) {
        $this->currentTargetTermName = $term_name;
      } else {
        $term_name = $this->getDefaultTargetTermName();
        $this->setCurrentTargetTermName($term_name);
      }
    }

    return $this->currentTargetTermName;
  }
  
  public function setCurrentTargetTermName(string $term_name) {
//todo::now - Drupal::state()->delete
//todo::now - Drupal::state() - setCurrentTargetTermName
    if (!empty($term_name) && $term_name === $this->currentTargetTermName) {
      // nothing to do
      return $this->currentTargetTermName;
    }
    
    $vocabulary = $this->vocabulary;
    if ($term_name !== SlogTx::TARGETTERM_DEFAULT && !$vocabulary->isValidTargetTermName($term_name)) {
      $args = [
          '%vid' => $this->vocabulary->id(),
          '%name' => $term_name,
      ];
      $message = t('Target term name %name not valid for vocabulary: %vid.', $args);
      throw new \LogicException($message);
    }
    
    $this->currentTargetTermName = $term_name;
    $key = $this->getStateKeyCurrentTargetTermName();
    if ($term_name === SlogTx::TARGETTERM_DEFAULT) {
      \Drupal::state()->delete($key);
    } else {
      \Drupal::state()->set($key, $term_name);
    }
    
    // reset vocabulary's local caches
    $vocabulary->resetRootTermCache();

    return $this->currentTargetTermName;
  }

  /**
   * {@inheritdoc}
   */
  public function isValidTargetTermId($target_term_id, &$err_msg) {
    return $this->targetTermExists($target_term_id, $err_msg);
  }

  /**
   * {@inheritdoc}
   */
  public function buildTargetTermName($entity_id = 0) {
    return SlogTx::TARGETTERM_DEFAULT;
  }

}
