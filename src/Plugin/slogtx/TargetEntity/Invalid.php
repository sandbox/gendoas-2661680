<?php

/**
 * @file
 * Contains \Drupal\slogtx\Plugin\slogtx\TargetEntity\Invalid.
 */

namespace Drupal\slogtx\Plugin\slogtx\TargetEntity;

use Drupal\slogtx\Plugin\slogtx\TargetEntityBase;
use Drupal\slogtx\SlogTx;

/**
 * @todo.
 *
 * @SlogtxTargetEntity(
 *   id = "invalid",
 *   title = @Translation("Invalid"),
 *   description = @Translation("This is an invalid target entity."),
 *   tbtab_edit_class = FALSE,
 *   entity_required = FALSE,
 *   lock_entity_required = TRUE,
 *   provider = "slogtx",
 * )
 */
class Invalid extends TargetEntityBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function isValid() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultTargetTermName() {
    return $this->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentTargetTermName() {
    return $this->getDefaultTargetTermName();
  }
  
  public function setCurrentTargetTermName(string $term_name) {
    $this->currentTargetTermName = $this->getDefaultTargetTermName();
    return $this->currentTargetTermName;
  }

  /**
   * {@inheritdoc}
   */
  public function isValidTargetTermId($target_term_id, &$err_msg) {
      return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildTargetTermName($entity_id = 0) {
    return $this->getDefaultTargetTermName();
  }

}
