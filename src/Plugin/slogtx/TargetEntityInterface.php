<?php

/**
 * @file
 * Contains \Drupal\slogtx\Plugin\slogtx\TargetEntityInterface.
 */

namespace Drupal\slogtx\Plugin\slogtx;

/**
 * Defines the interface for target entity plugins.
 */
interface TargetEntityInterface {
  
  /**
   * Save the plugin settings.
   * 
   * @param array $settings
   *  The settings of an individual plugin
   * @return Drupal\slogtx\Plugin\slogtx\TargetEntityInterface
   *  The plugin instans itself.
   */
  public function saveSettings(array $settings);
  
  /**
   * Return the default target term name.
   * 
   * Default target term name may be dependent on the current entity:
   *  - Role - current default role
   *  - User - current user
   */
  public function getDefaultTargetTermName();
  
  /**
   * Return the prefix for an entity dependent target term.
   * 
   * - E.g. 'Node' for node target entity, UserRole for role target entity, ...
   * - If target_term_prefix is empty, the plugin id as CamelCase is returned
   * 
   * @return string
   */
  public function getTargetTermPrefix();
  
  /**
   * Whether the target term is valid by the target entity.
   * 
   * - E.g. for node target term the associated node has to exist.
   * - Do not confuse with TxVocabulary::isValidTargetTermName(),
   *   reference to the target entity is not validated there.
   * 
   * @param integer $target_term_id
   * @param string $err_msg
   *  Return an error massage if not valid
   * @return boolean
   *  TRUE if the target entity exists, FALSE otherwise.
   * 
   * @throws \LogicException
   *   Thrown when vocabulary instance is not set.
   */
  public function isValidTargetTermId($target_term_id, &$err_msg);

  /**
   * Return an unique target term name.
   * 
   * The name is built upon the target term prefix and the entity id.
   * 
   * @param integer|string $entity_id
   */
  public function buildTargetTermName($entity_id = 0);
  
  
  /**
   * Return the entity id from the target term name.
   * 
   * @param string $name
   *  The name of tre target term
   * @return string|integer
   *  The target entity id.
   */
  public function getTargetEntityId($name);
  
  /**
   * Return data for all required target terms.
   * 
   * - For example the target entity 'none' needs the default target term,
   * - Other target entities usually don't need initial target terms,
   *   e.g. on demand for a node entity there is to create a target term
   *   with name = 'Node.123' and all data for this node are inside this target term
   * 
   * @return array
   *  Each Element as an array with data for creating a required target term.
   * 
   */
  public function getRequiredTargetTerms();
  
  /**
   * Magic method: Gets a property value.
   *
   * @param $property_name
   *   The name of the property to get; e.g., 'status'.
   *
   * @return mixed
   *   The property value.
   */
  public function __get($property_name);

  /**
   * Magic method: Sets a property value.
   *
   * @param $property_name
   *   The name of the property to set; e.g., 'status'.
   * @param $value
   *   The value to set, or NULL to unset the property.
   */
  public function __set($property_name, $value);

}
