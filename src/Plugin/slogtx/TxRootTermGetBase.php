<?php

/**
 * @file
 * Contains \Drupal\slogtx\Plugin\slogtx\TxRootTermGetBase.
 */

namespace Drupal\slogtx\Plugin\slogtx;

use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\slogtx\Interfaces\TxToolbarInterface;
use Drupal\slogtx\Interfaces\TxVocabularyInterface;
use Drupal\slogtx\Interfaces\TxTermInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\PluginBase;

/**
 * Defines the base class for root term get plugins.
 */
abstract class TxRootTermGetBase extends PluginBase implements TxRootTermGetInterface {

  protected $toolbar;
  protected $vocabulary;
  protected $target_term;
  protected $default_root_term;

  /**
   * Overridable function used in constructor only
   * 
   * Since toolbar is required a valid toolbar id must be set 
   * either by this function or by $configuration
   * 
   * @return string|boolean (optional) defaults to FALSE
   */
  protected function _getBaseToolbarID() {
    return FALSE;
  }

  /**
   * Overridable function used in constructor only
   * 
   * On calling this function there is a toolbar instance set already,
   * by overriding additional validation can be done.
   * 
   * @return boolean (optional) defaults to TRUE
   */
  protected function _isValidToolbar() {
    return TRUE;
  }

  /**
   * Overridable function used in constructor only
   * 
   * Vocabulary id is composed by toolbar id and plugin id.
   * 
   * @return string
   */
  protected function _buildVocabularyId() {
    $base_tbid = $this->getBaseToolbarId();
    $plugin_id = $this->getPluginId();
    return SlogTx::getVocabularyIdFromParts([$base_tbid, $plugin_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $options = $configuration['rtget-options'] ?? [];
    unset($configuration['rtget-options']);
    $target_tbid = $plugin_definition['toolbar_id'] ?? '';
    
    // create for role (not for others like user, _node)
    if ($target_tbid === 'role') {
      $options['do_create'] = TRUE;
      if (empty($options['entity'])) {
        $options['entity'] = SlogXt::getDefaultRole();
      }
    }
    elseif ($target_tbid === 'user' && empty($options['entity'])) {
      $options['entity'] = SlogXt::getUserEntity();
    }
    
    $configuration += $plugin_definition;
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    if (empty($configuration['base_toolbar'])) {
      $configuration['base_toolbar'] = $this->_getBaseToolbarID();
    }

    $this->toolbar = SlogTx::getToolbar($configuration['base_toolbar'], TRUE);
    if (!$this->toolbar || !$this->toolbar instanceof TxToolbarInterface) {
      throw new \LogicException(t('Base toolbar not set.'));
    }
    if (!$this->_isValidToolbar()) {
      throw new \LogicException(t('Base toolbar not valid.'));
    }

    // add vocabulary, ensure vocabulary exists
    $vid = $configuration['vocabulary_id'] ?? $this->_buildVocabularyId();
    $entity = $options['entity'] ?? SlogTx::TARGETENTITY_ID_NONE;
    $new_data = [
        'provider' => $this->getProvider(),
        'description' => $configuration['vocabulary_description'] ?? '',
        'icon_id' => $configuration['icon_id'],
        'weight' => $configuration['weight'],
        'target_entity_plugin_id' => $this->_getNewTargetEntiyId($vid, $entity),
    ];
    //target_entity_plugin_id
    $vocabulary = SlogTx::getVocabulary($vid, FALSE, $new_data);  // ensure vocabulary
    if (!$vocabulary || !$vocabulary instanceof TxVocabularyInterface) {
      $args = [
          '@found' => $new_data['create_new'] ? t('created') : t('found'),
          '@vid' => $vid,
      ];
      throw new \LogicException(t('Vocabulary not @found: @vid', $args));
    }

    $this->setVocabulary($vocabulary);
    $tt_name = $options['tt_name'] ?? SlogTx::TARGETTERM_DEFAULT;
    if ($entity && $entity instanceof EntityInterface) {
      $vocabulary->setCurrentTargetEntity($entity);
      $tt_name = $vocabulary->getCurrentTargetTermName();
    }

    //
    $do_create = (boolean) ($options['do_create'] ?? FALSE);
    $this->setTargetTermByName($tt_name, $do_create);
  }

  /**
   * 
   */
  private function _getNewTargetEntiyId($vid, $entity) {
    $enforced = $this->toolbar->getEnforceTargetEntity();
    $te_plugin_id = $entity;
    if (is_object($entity) && $entity instanceof EntityInterface) {
      $te_plugin_id = $entity->getEntityTypeId();
    }

    if ($enforced && $enforced !== $te_plugin_id) {
      $args = [
          '@vid' => $vid,
          '@req' => $enforced,
          '@set' => $te_plugin_id,
      ];
      throw new \LogicException(t('Target entity missmatch: @vid/@req/@set'), $args);
    }

    return $te_plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseToolbar() {
    return $this->toolbar;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseToolbarId() {
    return $this->toolbar->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabulary() {
    if (!$this->vocabulary || !$this->vocabulary instanceof TxVocabularyInterface) {
      if ($throw) {
        $vid = $this->_buildVocabularyId();
        throw new \LogicException(t('Vocabulary does not exist: @vid'), ['@vid' => $vid]);
      }
      return FALSE;
    }
    return $this->vocabulary;
  }

  /**
   * {@inheritdoc}
   */
  protected function setVocabulary(TxVocabularyInterface $vocabulary) {
    if ($vocabulary->getToolbarId() !== $this->getBaseToolbarId()) {
      throw new \LogicException(t('Vocabulary / base toolbar missmatch.'));
    }
    $this->vocabulary = $vocabulary;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetTerm() {
    if (!$this->target_term) {
      $vocabulary = $this->getVocabulary();
      $this->target_term = $vocabulary->getTargetTermByName(SlogTx::TARGETTERM_DEFAULT);
    }
    return $this->target_term;
  }

  /**
   * {@inheritdoc}
   */
  public function hasTargetTerm() {
    return !empty($this->target_term);
  }

  /**
   * {@inheritdoc}
   */
  public function hasTargetTermByName($tt_name) {
    $target_term = $this->getTargetTerm();
    if ($target_term) {
      return ($target_term->label() === $tt_name);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetTerm(TxTermInterface $target_term) {
    if (!$target_term->isTargetTerm()) {
      throw new \LogicException(t('Invalid target term.'));
    }
    $vocabulary = $this->getVocabulary();
    if ($vocabulary->id() !== $target_term->getVocabularyId()) {
      throw new \LogicException(t('Target term / vocabulary missmatch.'));
    }

    $vocabulary->setCurrentTargetTermName($target_term->label());
    $this->target_term = $target_term;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetTermByName($tt_name = NULL, $do_create = FALSE) {
    $vocabulary = $this->getVocabulary();
    if (empty($tt_name)) {
      // ensure current tt_name ist prepared, e.g. by setCurrentTargetEntity()
      $tt_name = $vocabulary->getCurrentTargetTermName();
    }
    $target_term = $vocabulary->getTargetTermByName($tt_name, TRUE, $do_create);
    if ($target_term) {
      $this->setTargetTerm($target_term);
    }
    else {
      $this->target_term = NULL;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function _getDefaultRootTerm() {
    if ($this->hasTargetTerm()) {
      if (empty($this->default_root_term)) {
        $vocabulary = $this->getVocabulary();
        $vocabulary->setCurrentRootTermName(SlogTx::ROOTTERM_DEFAULT);
        $this->default_root_term = $vocabulary->getCurrentRootTerm();
        if (empty($this->default_root_term)) {
          throw new \LogicException(t('Default root term not found.'));
        }
      }
      return $this->default_root_term;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRootTermByName($rt_name = SlogTx::ROOTTERM_DEFAULT, $do_create = TRUE) {
    if ($this->hasTargetTerm()) {
      if ($rootterm = $this->_getDefaultRootTerm()->getSiblingRootTerm($rt_name, $do_create)) {
        // ensure valid current root term 
        $this->getVocabulary()->setCurrentRootTermName($rt_name);
      }
    }
    return ($rootterm ?? FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function hasRootTermByName($rt_name) {
    $rootterm = $this->_getDefaultRootTerm()->getSiblingRootTerm($rt_name, FALSE);
    return !empty($rootterm);
  }

  public function getProvider() {
    return $this->get('provider');
  }

  public function get($key = NULL) {
    if (empty($key)) {
      return $this->configuration;
    }
    elseif (isset($this->configuration[$key])) {
      return $this->configuration[$key];
    }
  }

}
