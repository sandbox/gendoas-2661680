<?php

/**
 * @file
 * Contains \Drupal\slogtx\Plugin\slogtx\TxRootTermGetInterface.
 */

namespace Drupal\slogtx\Plugin\slogtx;

use Drupal\slogtx\SlogTx;
use Drupal\slogtx\Interfaces\TxVocabularyInterface;
use Drupal\slogtx\Interfaces\TxTermInterface;

/**
 * Defines the interface for target entity plugins.
 */
interface TxRootTermGetInterface {
  
  public function getBaseToolbar();	
  public function getBaseToolbarId();	
  public function getVocabulary();	
  public function getTargetTerm();	
  public function hasTargetTermByName($tt_name);	
  public function setTargetTerm(TxTermInterface $target_term);	
  public function setTargetTermByName($tt_name = NULL, $do_create = FALSE);	
  public function getRootTermByName($rt_name = SlogTx::ROOTTERM_DEFAULT, $do_create = TRUE);	
  public function hasRootTermByName($rt_name);
	  
  
}
