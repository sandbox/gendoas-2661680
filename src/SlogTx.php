<?php

/**
 * @file
 * Contains \Drupal\slogtx\SlogTx.
 */

namespace Drupal\slogtx;

use Drupal\slogtx\TxEntityLockTrait;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\slogtx\Entity\TxToolbar;
use Drupal\slogtx\Entity\TxVocabulary;
use Drupal\slogtx\Entity\MenuTerm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\Utility\NestedArray;
use Symfony\Component\DependencyInjection\Container;

/**
 * Static helper functions and static service container wrapper for slogtx_voc module.
 */
class SlogTx {

  use TxEntityLockTrait;

  /**
   * Term is disabled.
   */
  const DISABLED = 0;

  /**
   * Term is enabled.
   */
  const ENABLED = 1;

  /**
   * Machine name parts delimiter of vocabulary ids.
   * 
   * Vocabulary id is composed by toolbar id and an unique vocabulary id
   * within the toolbar, separated by SLOG_NAME_DELIMITER.
   */
  const SLOG_NAME_DELIMITER = '__';

  /**
   * Default target term name.
   */
  const TARGETTERM_DEFAULT = 'Default';

  /**
   * Default root term name.
   */
  const ROOTTERM_DEFAULT = 'Default';

  /**
   * Draft root term name.
   */
  const ROOTTERM_DRAFT = 'Draft';

  /**
   * Maximal depth of vocabulary items.
   */
  const TERMS_MAX_DEPTH = 2;

  /**
   * 
   */
  const TARGETENTITY_ID_INVALID = 'invalid';

  /**
   * 
   */
  const TARGETENTITY_ID_NONE = 'none';

  /**
   * 
   */
  const TOOLBAR_UNDERSCORE = '_';

  /**
   * The id of the system toolbar.
   */
  const TOOLBAR_ID_SYS = '_sys';

  /**
   * The id of the node subsystem toolbar.
   */
  const TOOLBAR_ID_NODE_SUBSYS = '_node';

  /**
   * The id of the submenu vocabulary within the _node toolbar.
   */
  const SYS_VOCAB_ID_SUBMENU = 'submenu';

  /**
   * The id of the collaborate vocabulary within the _node toolbar.
   */
  const SYS_VOCAB_ID_COLLABORATE = 'collaborate';

  /**
   * 
   */
  const TBTAB_SYSID_DUMMY = 'dummy';

  /**
   * 
   */
  const TXMENUTOOLBAR = 'txmenutoolbar';

  /**
   * Cache retrieved logger objects for channels
   *
   * @var array of \Psr\Log\LoggerInterface objects
   */
  protected static $cacheLogger = [];

  /**
   */
  protected static $cacheInstances = [];
  protected static $txInstanceLoading = [];
  protected static $cacheTbSorted = NULL;
  protected static $cacheToolbarsVisible = NULL;
  protected static $config = NULL;

  /**
   * Cashes already loaded storages.
   *
   * @var array of storage objects. 
   */
  protected static $storage = [];

  /**
   * Cashes already retrieved lockable entity types.
   *
   * @var array of entity type objects. 
   */
  protected static $lockableEntityTypes;

  /**
   * Data once gatherd by throwing events
   *
   * @var array
   *  Each item with data from a single event gathering.
   */
  public static $event_result = [];

  /**
   * Allow to make some data global.
   * 
   * This data are prepared for client side (JS).
   *
   * @var array 
   */
  protected static $toolbartab_data = [];

  /**
   * Cleanup for uninstalling slogtx module.
   * 
   * Cleanups:
   *  - Delete all status and locking states for all entity types
   *  - Delete settings for target entity plugins
   *  - Delete all slog toolbars, vocabularies and terms
   * 
   * Use this for uninstalling or testing slogtx module only,
   * since it deletes required entities and other entries. 
   */
  public static function cleanupTotalForUninstall() {
    // delete all lock records
    $keys = self::dbGetLockedKeys('slogtx.locked.%');
    self::getLockedStorage()->deleteMultiple($keys);

    // delete settings for all target entity plugins
    self::pluginManager('target_entity')->deleteAllSettings();

    // delete all entities: slog toolbars, vocabularies and terms
    foreach (self::getToolbars() as $toolbar) {
      $toolbar->delete();
    }

    // delete all taxonomy vocabularies too
    // it is not possible to uninstall slogtx with existing terms
    foreach (self::entityStorage('taxonomy_vocabulary')->loadMultiple() as $vocabulary) {
      $vocabulary->delete();
    }
  }

  /**
   * Return all locked entries with given search pattern.
   * 
   * - Searching in 'key_value' table with collection=='state'
   * - Search in 'name' field
   * - For 'start with' needs '%' appended to the search pattern
   * 
   * @param string $search
   *  Search pattern for the query
   * @return array 
   *  An array with all found names
   */
  public static function dbGetLockedKeys($search) {
    return \Drupal::database()->select('key_value', 'kv')
                    ->fields('kv', ['name'])
                    ->condition('kv.collection', 'slogtx.locked')
                    ->condition('kv.name', $search, 'LIKE')
                    ->execute()
                    ->fetchCol();
  }

  /**
   * Returns a key/value storage for slogtx.locked collection.
   *
   * @return \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  public static function getLockedLabels($term_type_id) {
    if ($term_type_id === 'slogtx_rt') {
      return [self::ROOTTERM_DEFAULT, self::ROOTTERM_DRAFT];
    }
    elseif ($term_type_id === 'slogtx_tt') {
      return [self::TARGETTERM_DEFAULT];
    }
    return [];
  }

  public static function getLockedStorage() {
    return \Drupal::keyValue('slogtx.locked');
  }

  public static function dbGetValidTids($tids, &$invalid_tids = NULL) {
    if (empty($tids)) {
      return [];
    }
    
    $valid_tids = \Drupal::database()->select('taxonomy_term_data', 'td')
            ->fields('td', ['tid'])
            ->condition('tid', $tids, 'IN')
            ->execute()
            ->fetchCol();

    $invalid_tids = array_diff($tids, $valid_tids);
    return $valid_tids;
  }

  /**
   * Returns an entity storage object.
   * 
   * @param string $entity_type_id
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The storage object associated with the specified entity type id.
   */
  public static function entityStorage($entity_type_id) {
    if (!isset(self::$storage[$entity_type_id])) {
      self::$storage[$entity_type_id] = \Drupal::entityTypeManager()->getStorage($entity_type_id);
    }
    return self::$storage[$entity_type_id];
  }

  public static function getConfig() {
    if (empty(self::$config)) {
      self::$config = \Drupal::config('slogtx.settings');
    }
    return self::$config;
  }

  public static function getMaxToolbarTabs() {
    return (integer) (self::getConfig()->get('max_toolbar_tabs') ?? 4);
  }

  /**
   * Whether the term with the term id exists.
   * 
   * @param integer $tid
   * @return boolean
   */
  public static function termExists($tid) {
    $return = \Drupal::entityQuery('taxonomy_term')
            ->condition('tid', $tid)
            ->execute();

    return !empty($return);
  }

  public static function setEntityInstance($entity_type_id, $entity_id, EntityInterface $instance) {
    if (!defined('MAINTENANCE_MODE') && !empty($entity_id) && !empty($entity_type_id)) {
      if (!empty(self::$txInstanceLoading[$entity_type_id][$entity_id])) {
        if (!isset(self::$cacheInstances[$entity_type_id])) {
          self::$cacheInstances[$entity_type_id] = [];
        }
        self::$cacheInstances[$entity_type_id][$entity_id] = $instance;
      }
    }
  }

  /**
   * Return an entity object of $entity_type_id and $entity_id.
   * 
   * If $entity_id is an object itself this object is returned.
   * 
   * @param string $entity_type_id
   * @param string|object $entity_id
   * @param boolean $reset optional, defaults to FALSE
   * @return object
   */
  public static function entityInstance($entity_type_id, $entity_id, $reset = FALSE) {
    if (!isset(self::$cacheInstances[$entity_type_id])) {
      self::$cacheInstances[$entity_type_id] = [];
    }
    $instance = $entity_id;
    if (is_string($entity_id) || is_numeric($entity_id)) {
      if ($reset) {
        unset(self::$cacheInstances[$entity_type_id][$entity_id]);
      }
      if (!isset(self::$cacheInstances[$entity_type_id][$entity_id])) {
        if (!isset(self::$txInstanceLoading[$entity_type_id])) {
          self::$txInstanceLoading[$entity_type_id] = [];
        }
        if (empty(self::$txInstanceLoading[$entity_type_id][$entity_id])) {
          self::$txInstanceLoading[$entity_type_id][$entity_id] = TRUE;
          $instance = self::entityStorage($entity_type_id)->load($entity_id);
          self::$cacheInstances[$entity_type_id][$entity_id] = $instance;
          unset(self::$txInstanceLoading[$entity_type_id][$entity_id]);
        }
      }
    }
    elseif ($instance instanceof EntityInterface && $entity_id = $instance->id()) {
      self::$cacheInstances[$entity_type_id][$entity_id] = $instance;
    }

    return self::$cacheInstances[$entity_type_id][$entity_id];
  }

  /**
   * Return an entity object from $route_match and $entity_type_id.
   * 
   * If $route_match provides an object itself this object is returned.
   * 
   * @param RouteMatchInterface $route_match
   * @param string $entity_type_id
   * @return object
   */
  public static function entityInstanceByRoute(RouteMatchInterface $route_match, $entity_type_id) {
    $entity = $route_match->getParameter($entity_type_id);
    return self::entityInstance($entity_type_id, $entity);
  }

  /**
   * Returns ids of all slog lockable entity types.
   * 
   * @return array of entity types.
   */
  public static function lockableEntityTypes() {
    if (!isset(self::$lockableEntityTypes)) {
      $definitions = \Drupal::entityTypeManager()->getDefinitions();
      $allowed = self::getAllAllowedOperations();

      $entity_types = [];
      foreach ($definitions as $definition) {
        $slog_is_lockable = $definition->get('slog_is_lockable');
        if ($slog_is_lockable && !empty($slog_is_lockable)) {
          $operations = ($slog_is_lockable === true) //
                  ? ['delete'] : $slog_is_lockable;
          if (is_array($operations)) {
            $operations = array_intersect($allowed, $operations);
            if (!empty($operations)) {
              $definition->set('slog_is_lockable', $operations);
              $entity_types[$definition->id()] = $definition;
            }
          }
        }
      }

      self::$lockableEntityTypes = $entity_types;
    }

    return self::$lockableEntityTypes;
  }

  /**
   * Returns ids of all slog lockable entity types.
   * 
   * @return array of entity type ids.
   */
  public static function lockableEntityTypeIds() {
    $entity_types = self::lockableEntityTypes();
    return array_keys($entity_types);
  }

  /**
   * Returns whether an entity type is lockable.
   * 
   * @param string $entity_type_id
   * @return boolean
   *   TRUE if is lockable at all, i.e. undependent of the operations
   */
  public static function isEntityTypeLockable($entity_type_id) {
    $entity_types = self::lockableEntityTypes();
    return isset($entity_types[$entity_type_id]);
  }

  /**
   * Returns toolbar ids.
   * 
   * @param boolean $all
   *  If TRUE, return ids for all toolbars, independent of status.
   * @param boolean $status
   *  If $all==FALSE, return ids with the specified status.
   * @return array of toolbars ids.
   */
  public static function getToolbarIds($all = FALSE, $status = TRUE) {
    $query = self::entityStorage('slogtx_tb')->getQuery();
    if (!$all) {
      $query->condition('status', $status);
    }
    return $query->execute();
  }

  /**
   * Return an array of toolbar objects.
   * 
   * @param boolean $all
   *  Return all toolbars if TRUE, the toolbars with the status otherwise. 
   *  If TRUE return all toolbar objects, independent of status.
   * @param boolean $status
   *  If $all==FALSE, return toolbar objects with the specified status.
   * @return array of toolbar objects.
   *   \Drupal\slogtx\Interfaces\TxToolbarInterface
   * 
   * @see self::getToolbarIds()
   */
  public static function getToolbars($all = FALSE, $status = TRUE) {
    $toolbars = self::entityStorage('slogtx_tb')->loadMultiple(self::getToolbarIds($all, $status));
    ksort($toolbars);
    return $toolbars;
  }

  public static function resetToolbarsSorted($all = FALSE, $status = TRUE) {
    self::$cacheTbSorted = NULL;
  }

  public static function getToolbarsSorted($all = FALSE, $status = TRUE) {
    if (!self::$cacheTbSorted) {
      $toolbars = self::getToolbars($all, $status);
      krsort($toolbars);

      $tmp_list = [];
      $frac = 1000;
      foreach ($toolbars as $toolbar_id => $toolbar) {
        $weight = $toolbar->getWeight() + 5000000;
        $idx = sprintf("%'.07d", $weight) . '.' . --$frac;
        $tmp_list[$idx] = $toolbar;
      }
      ksort($tmp_list, SORT_NUMERIC);

      $toolbars = [];
      foreach ($tmp_list as $idx => $toolbar) {
        $toolbars[$toolbar->id()] = $toolbar;
      }

      self::$cacheTbSorted = $toolbars;
    }

    return self::$cacheTbSorted;
  }

  public static function getMenuToolbars() {
    $toolbars = self::getToolbars(TRUE);
    $toolbars = array_filter($toolbars, function ($toolbar) {
      return $toolbar->isMenuToolbar();
    });

    return $toolbars;
  }

  /**
   * Whether toolbar is visible as menu (exclude underscored).
   * 
   * - For visability: enabled and has menu items.
   * - Underscored: i.g. _sys, _node
   * 
   * @return array of toolbars
   */
  public static function getToolbarsVisible() {
    if (!isset(self::$cacheToolbarsVisible)) {
      self::$cacheToolbarsVisible = [];
      $toolbars = SlogTx::getToolbars();
      foreach ($toolbars as $toolbar_id => $toolbar) {
        if (!$toolbar->isUnderscoreToolbar() && $toolbar->hasVisibleVocabularies()) {
          self::$cacheToolbarsVisible[$toolbar_id] = $toolbar;
        }
      }
    }

    return self::$cacheToolbarsVisible;
  }

  /**
   * Return as manu visible toolbars without 'user' and 'role'.
   * 
   * @return array of Toolbars
   */
  public static function getToolbarsVisibleGlobal() {
//    return array_diff_key(self::getToolbarsVisible(), ['user' => 0, 'role' => 0]);
    $visible = self::getToolbarsVisible();
    $visible_global = array_filter($visible, function ($toolbar) {
      return $toolbar->isGlobalMenuToolbar();
    });
    return $visible_global;
  }

  public static function isToolbarVisibleGlobal($toolbar_id) {
    $vg_tbs = SlogTx::getToolbarsVisibleGlobal();
    return !empty($vg_tbs[$toolbar_id]);
  }

  /**
   * Return a toolbar object.
   * 
   * @param string $toolbar_id
   * @param boolean $reset optional, defaults to FALSE
   * @return Drupal\slogtx\Interfaces\TxToolbarInterface
   */
  public static function getToolbar($toolbar_id, $reset = FALSE) {
    return self::entityInstance('slogtx_tb', $toolbar_id, $reset);
  }

  /**
   * Return the ids of all vocabularies, unsorted.
   * 
   * @return array of strings
   *   An associative array with key equals values.
   */
  public static function getAllVocabularyIds() {
    return self::entityStorage('slogtx_voc')->getQuery()->execute();
  }

  /**
   * Return all vocabularies as object, unsorted.
   * 
   * @return array of Drupal\slogtx\Interfaces\TxVocabularyInterface
   *   An associative array with key equals vocabulary id.
   */
  public static function getAllVocabularies() {
    return self::entityStorage('slogtx_voc')->loadMultiple(self::getAllVocabularyIds());
  }

  /**
   * Return a vocabulary object.
   * 
   * @param string $vid
   * @param boolean $reset optional, defaults to FALSE
   * @return Drupal\slogtx\Interfaces\TxVocabularyInterface
   */
  public static function getVocabulary($vid, $reset = FALSE, &$new_data = FALSE) {
    $vocabulary = self::entityInstance('slogtx_voc', $vid, $reset);
    if (!$vocabulary && is_array($new_data)) {
      $new_data['create_new'] = TRUE;
      $vocabulary = self::ensureVocabulary($vid, $new_data, TRUE);
    }
    else {
      $new_data['create_new'] = FALSE;
    }
    return $vocabulary;
  }

  public static function getVocabulariesByToolbar($toolbar_id, $all = FALSE, $status = TRUE) {
    if ($toolbar = self::getToolbar($toolbar_id)) {
      return $toolbar->getVocabularies($all, $status);
    }
    return [];
  }

  public static function getTargetTerm($tid, $reset = FALSE) {
    return self::entityInstance('slogtx_tt', $tid, $reset);
  }

  public static function getRootTerm($tid, $reset = FALSE) {
    return self::entityInstance('slogtx_rt', $tid, $reset);
  }

  public static function getMenuTerm($tid, $reset = FALSE) {
    return self::entityInstance('slogtx_mt', $tid, $reset);
  }

  public static function ensureToolbar($toolbar_id, $provider = 'slogtx', $syslock = FALSE) {
    $toolbar = self::getToolbar($toolbar_id);
    if (!$toolbar) {
      $values = [
          'id' => $toolbar_id,
          'name' => $toolbar_id,
          'description' => 'Auto generated toolbar.',
          'provider' => $provider,
      ];
      if ($syslock) {
        $values['description'] .= ' Locked for deleting und disabling';
        $values['locked'] = ['delete', 'disable'];
      }
      TxToolbar::create($values)->save();
      $toolbar = self::getToolbar($toolbar_id, TRUE);
    }

    return $toolbar;
  }

  public static function ensureVocabulary($vid, $data = [], $syslock = FALSE) {
    if (!self::isVocabularyId($vid)) {
      $message = t('This is not a valid vocabulary id:  @vid.', ['@vid' => $vid]);
      throw new \LogicException($message);
    }
    $vocabulary = self::getVocabulary($vid);
    if (!$vocabulary) {
      list($toolbar_id, $toolbartab) = self::getVocabularyIdParts($vid);
      $data += [
          'name' => Container::camelize($toolbartab),
          'target_entity_plugin_id' => self::TARGETENTITY_ID_NONE,
          'provider' => 'slogtx',
      ];
      if (empty($data['description'])) {
        $data['description'] = t('Auto generated slog vocabulary.');
      }

      if (is_array($syslock)) {
        $locked = $syslock;
        $syslock = FALSE;
        if (in_array('delete', $locked)) {
          $data['description'] .= ' ' . t('Locked for deleting.');
        }
      }
      elseif ($syslock) {
        $data['description'] .= ' ' . t('Locked for deleting und disabling.');
      }
      $toolbar = self::ensureToolbar($toolbar_id, $data['provider'], $syslock);
    }
    if (!$vocabulary && $toolbar) {
      if ($enforce_teid = $toolbar->getEnforceTargetEntity()) {
        $data['target_entity_plugin_id'] = $enforce_teid;
      }

      $values = [
          'vid' => $vid,
          'name' => $data['name'],
          'description' => $data['description'],
          'bundle' => $toolbar_id,
          'target_entity_plugin_id' => $data['target_entity_plugin_id'],
          'provider' => $data['provider'],
          'weight' => (integer) $data['weight'],
      ];
      if (!empty($data['icon_id'])) {
        $values['icon_id'] = $data['icon_id'];
      }
      if ($syslock) {
        $values['locked'] = ['delete', 'disable'];
      }
      elseif (!empty($locked) && is_array($locked)) {
        $values['locked'] = $locked;
      }
      TxVocabulary::create($values)->save();
      $vocabulary = self::getVocabulary($vid, TRUE);
    }

    return $vocabulary;
  }

  /**
   * Return target entity plugin objects.
   * 
   * @see \Drupal\slogtx\TargetEntityPluginManager::getTargetEntityPlugins()
   */
  public static function getTargetEntityPlugins($all = FALSE, $status = TRUE) {
    return self::pluginManager('target_entity')->getTargetEntityPlugins($all, $status);
  }

  /**
   * Return the target entity plugin object for a given plugin id.
   * 
   * @see \Drupal\slogtx\TargetEntityPluginManager::getTargetEntityPlugin()
   */
  public static function getTargetEntityPlugin($plugin_id) {
    return self::pluginManager('target_entity')->getTargetEntityPlugin($plugin_id);
  }

  /**
   * Test if this is a valid and enabled target entity id.
   * 
   * @param string $target_entity_id
   * @return boolean
   */
  public static function hasTargetEntityId($target_entity_id) {
    $plugin = self::pluginManager('target_entity')->getTargetEntityPlugin($target_entity_id);
    return $plugin ? ($plugin->status && $plugin->isValid()) : FALSE;
  }

  /**
   * Return the default hint for 'entity required'.
   * 
   * @return string
   */
  public static function defaultEntityRequiredHint() {
    return t('An entity must be specified');
  }

  //
  // Sys Base
  //
  public static function getTbMenuRootTerm($plugin_id, $entity, $do_create = FALSE, $rt_name = self::ROOTTERM_DEFAULT) {
    $options = [
        'plugin_id' => $plugin_id,
        'entity' => $entity,
        'do_create' => $do_create,
    ];
    $rtget_plugin = self::pluginManager('rtget_tb_menu')->getInstance($options);
    return $rtget_plugin->getRootTermByName($rt_name, $do_create);
  }

  //
  // Sys Base
  //
  public static function getSysSysRtgetPlugin($plugin_id, $entity, $do_create = FALSE) {
    $options = [
        'plugin_id' => $plugin_id,
        'entity' => $entity,
        'do_create' => $do_create,
    ];
    return self::pluginManager('rtget_sys_sys')->getInstance($options);
  }

  public static function getSysSysRootTerm($plugin_id, $entity, $do_create = FALSE, $rt_name = self::ROOTTERM_DEFAULT) {
    $rtget_plugin = self::getSysSysRtgetPlugin($plugin_id, $entity, $do_create);
    return $rtget_plugin->getRootTermByName($rt_name, $do_create);
  }

  //
  // Sys Dummy
  //
//todo::next - dummy menu term - what ist the dummy menu term for ????
//          public static function getSysSysRootTermDummy() {
//            $plugin_id = self::TBTAB_SYSID_DUMMY;
//            return self::getSysSysRootTerm($plugin_id, FALSE, $do_create = TRUE);
//          }

  //
  // Node Base
  //
  public static function getSysNodeRootTerm($plugin_id, $node_id, $do_create = FALSE, $rt_name = self::ROOTTERM_DEFAULT) {
    $node = self::entityInstance('node', $node_id);
    $options = [
        'plugin_id' => $plugin_id,
        'entity' => $node,
        'do_create' => $do_create,
    ];
    $rtget_plugin = self::pluginManager('rtget_sys_node')->getInstance($options);
    return $rtget_plugin->getRootTermByName($rt_name, $do_create);
  }

  public static function hasSysNodeRootTerm($plugin_id, $node_id, $rt_name = self::ROOTTERM_DEFAULT) {
    $rootterm = self::getSysNodeRootTerm($plugin_id, $node_id, FALSE, $rt_name);
    return (!empty($rootterm));
  }

  //
  // Node Submenu
  //
  public static function getSysNodeRootTermSubmenu($node_id, $do_create = FALSE, $rt_name = self::ROOTTERM_DEFAULT) {
    $plugin_id = self::SYS_VOCAB_ID_SUBMENU;
    return self::getSysNodeRootTerm($plugin_id, $node_id, $do_create, $rt_name);
  }

  public static function hasSysNodeRootTermSubmenu($node_id, $rt_name = self::ROOTTERM_DEFAULT) {
    $plugin_id = self::SYS_VOCAB_ID_SUBMENU;
    return self::hasSysNodeRootTerm($plugin_id, $node_id, $rt_name);
  }

  //
  //node Collaborate
  //
  public static function getSysNodeRootTermCollaborate($node_id, $do_create = FALSE, $rt_name = self::ROOTTERM_DEFAULT) {
    $plugin_id = self::SYS_VOCAB_ID_COLLABORATE;
    return self::getSysNodeRootTerm($plugin_id, $node_id, $do_create, $rt_name);
  }

  public static function hasSysNodeRootTermCollaborate($node_id, $rt_name = self::ROOTTERM_DEFAULT) {
    $plugin_id = self::SYS_VOCAB_ID_COLLABORATE;
    return self::hasSysNodeRootTerm($plugin_id, $node_id, $rt_name);
  }

  /**
   * Returns the plugin manager for a certain slog plugin type.
   *
   * @param string $type
   *   The plugin type, e.g. target_entity.
   * @param string $provider
   *   The module that provides the manager, defaults to slogxt.
   *
   * @return object 
   *   A plugin manager instance.
   */
  public static function pluginManager($type, $provider = 'slogtx') {
    return \Drupal::service("plugin.manager.$provider.$type");
  }

  /**
   * Get definitions of a plugin type.
   * 
   * @param string $type
   *  The plugin type, e.g. target_entity.
   * 
   * @return array
   *  Collection of found definitions, e.g. slog vocabulary definitions.
   */
  public static function getDefinitions($type) {
    return self::pluginManager($type)->getDefinitions();
  }

  /**
   * Returns the definition for a plugin type and a plugin id
   * 
   * @param string $type
   *   The plugin type, e.g. target_entity.
   * 
   * @param string $plugin_id
   *  The plugin id, for target_entity e.g 'none'
   * 
   * @return array
   *  Data array for a plugin definition, e.g tbtop__favorit (vocabulary).
   */
  public static function getDefinition($type, $plugin_id) {
    return self::pluginManager($type)->getDefinition($plugin_id);
  }

  /**
   * Return the vocabulary id as an array of two pars.
   * 
   * The vocabulary id consists of two parts:
   *  - the toolbar id as the first part
   *  - an unique id within the toolbar as the second part
   *  - separated by the self::SLOG_NAME_DELIMITER ('__', double underline)
   * 
   * @param string $vid
   * @return array
   */
  public static function getVocabularyIdParts($vid) {
    return explode(self::SLOG_NAME_DELIMITER, $vid);
    ;
  }

  /**
   * Build the vocabulary is by the parts.
   * 
   * @param array $parts
   * @return string
   * 
   * @see self::getVocabularyIdParts()
   */
  public static function getVocabularyIdFromParts($parts) {
    if (is_array($parts) && count($parts) == 2) {
      return $parts[0] . self::SLOG_NAME_DELIMITER . $parts[1];
    }
    return '';
  }

  /**
   * Return the toolbar part of the vocabulary id.
   * 
   * @param string $vid
   * @return string
   * 
   * @see self::getVocabularyIdParts()
   */
  public static function getToolbarIdFromVid($vid) {
    if (self::isVocabularyId($vid)) {
      list($toolbar_id) = self::getVocabularyIdParts($vid);
      return $toolbar_id;
    }
  }

  /**
   * Return the vocabulary part of the vocabulary id.
   * 
   * @param string $vid
   * @return string
   * 
   * @see self::getVocabularyIdParts()
   */
  public static function getToolbartabFromVid($vid) {
    if (self::isVocabularyId($vid)) {
      list(, $toolbartab) = self::getVocabularyIdParts($vid);
      return $toolbartab;
    }
  }

  /**
   * Whether the id is a vocabulary id.
   * 
   * Checks if the vid consists of two parts, no more validation.
   * 
   * @param string $vid
   * @return boolean
   * 
   * @see self::getVocabularyIdParts()
   */
  public static function isVocabularyId($vid) {
    $parts = self::getVocabularyIdParts($vid);
    return (count($parts) == 2);
  }

  /**
   * Return toolbar data prepared SlogtbElement::buildToolbarTabItem()
   * 
   * @param string $toolbar_id
   * @return array
   */
  public static function getJsToolbarData($toolbar_id) {
    return self::$toolbartab_data[$toolbar_id] ?: [];
  }

  /**
   * Add toolbar data for later use
   * 
   * @see \Drupal\slogtb\Element\SlogtbElement::buildToolbarTabItem()
   * 
   * @param string $toolbar_id
   * @param array $data
   */
  public static function mergeJsToolbarData($toolbar_id, $data = NULL) {
    if (is_array($data)) {
      if (isset(self::$toolbartab_data[$toolbar_id])) {
        $pdata = self::$toolbartab_data[$toolbar_id];
        $data['hasMenus'] = (boolean) ($pdata['hasMenus'] ?? FALSE);
        $data['hasSubtrees'] = (boolean) ($pdata['hasSubtrees'] ?? FALSE);
        self::$toolbartab_data[$toolbar_id] = NestedArray::mergeDeepArray([$pdata, $data], TRUE);
      }
      else {
        self::$toolbartab_data[$toolbar_id] = $data;
      }
    }
    else {
      self::$toolbartab_data[$toolbar_id] = [];
    }
  }

  public static function deleteNodeVocabularies($nid) {
//todo::implement::deleteNodeVocabularies($nid)
  }

  public static function deleteUserVocabularies($uid) {
//todo::implement::deleteUserVocabularies($uid)
  }

  public static function deleteRoleVocabularies($rid) {
//todo::implement::deleteRoleVocabularies($rid)
  }

  /**
   * Return the tree of a root term or a menu term as parent.
   * 
   * @param string $vid
   *  Vocabulary ID
   * @param integer $parent_id
   *  The parent term id for which to get the menu tree.
   *  $parent_id is root term id or menu term id, but not target term id or 0.
   * @param integer $max_depth
   *  Menu depth of the menu tree
   * @param boolean $load_entities
   *  Load entities if TRUE
   * @return array|NULL
   *  Array of objects or definitions, or NULL on error
   */
  public static function getVocabularyMenuTree($vid, $parent_id, $max_depth = NULL, $load_entities = FALSE) {
    // Tree for target term or vocabulary are not supported.
    // There may be a huge amount of target terms
    $storage = self::entityStorage('slogtx_mt');
    return $storage->loadTree($vid, $parent_id, $max_depth, $load_entities);
  }

  /**
   * Return items sorted by path label.
   * 
   * @param \Drupal\slogtx\TxPathLabelInterface $items
   * @return array
   *  Sorted array if $items are instances of TxPathLabelInterface, unchanged otherwise.
   */
  public static function sortByPathLabel($items) {
    if (empty($items) || !is_array($items) || count($items) < 2) {
      return $items;
    }
    // ensure the items have path labels, test the first only
    $first_key = array_keys($items)[0];
    if (!($items[$first_key] instanceof \Drupal\slogtx\Interfaces\TxPathLabelInterface)) {
      return $items;
    }

    $sorted = [];
    $array = [];
    if (!empty($items)) {
      foreach ($items as $key => $item) {
        $array[$key] = $item->pathLabel();
      }
    }
    asort($array);
    foreach ($array as $key => $value) {
      $sorted[$key] = $items[$key];
    }

    return $sorted;
  }

  /**
   * Return root term or vocabulary for next prepare within the toolbar.
   * 
   * @param string $toolbar_id
   * @param EntityInterface $entity Optional, defaults to NULL.
   *  suitable entity for the toolbar (user_role, user)
   * @return root term or vocabulary object if found, otherwise FALSE
   */
  public static function getTbObjectToPrepare($toolbar_id, EntityInterface $entity = NULL, $do_create = FALSE) {
    if ($vocabs = self::getVocabulariesByToolbar($toolbar_id)) {
      $toolbar = self::getToolbar($toolbar_id);
      $is_global_menu_tb = $toolbar->isGlobalMenuToolbar();
      if ($is_global_menu_tb && !empty($entity)) {
        self::logger()->error("GlobalMenuToolbar missmatch: $toolbar_id");
        return FALSE;
      }

      $enforce_teid = $toolbar->getEnforceTargetEntity();
      foreach ($vocabs as $vid => $vocabulary) {
        if (!empty($enforce_teid) && $vocabulary->getTargetEnityTypeId() !== $enforce_teid) {
          self::logger()->error("TargetEnityTypeId missmatch: $vid");
          return FALSE;
        }

        $tePlugin = $vocabulary->getTargetEntityPlugin();
        if (!empty($entity)) {
          $tePlugin->setEntity($entity);
        }
        $te_entity = ($e = $tePlugin->getCurrentEntity()) ?: NULL;
        $plugin_id = self::getTxMenuToolbarPluginId($vid);
        $rootterm = self::getTbMenuRootTerm($plugin_id, $te_entity, ($do_create || $is_global_menu_tb));

        if (empty($rootterm)) {
          // target term and root term are not prepared yet,
          // but the vocabulary for the entity is preparable
          return $vocabulary;
        }
        elseif (!$vocabulary->currentHasMenuitems()) {
          return $rootterm;
        }
      }
    }

    return FALSE;
  }

  public static function getTxMenuToolbarPluginId($vid) {
    return self::TXMENUTOOLBAR . ':' . $vid;
  }

  /**
   * Whether there is a object to prepare (root term or vocabulary).
   * 
   * @param string $toolbar_id
   *  only very specific toolbars like 'role', 'user'
   * @param EntityInterface $entity Optional, defaults to NULL.
   *  suitable entity for the toolbar (user_role, user)
   * @return boolean
   */
  public static function hasTbObjectToPrepare($toolbar_id, EntityInterface $entity = NULL) {
    return (!empty(self::getTbObjectToPrepare($toolbar_id, $entity)));
  }

  /**
   * Create the initial menu item for the first preparable vocabulary.
   * 
   * @param string $toolbar
   * @param EntityInterface $entity Optional, defaults to NULL.
   */
  public static function prepareToolbarInitial($toolbar, EntityInterface $entity = NULL) {
    if (!empty($entity)) {
      $root_term = self::getTbObjectToPrepare($toolbar, $entity);
    }
    elseif (($vocabs = SlogTx::getVocabulariesByToolbar($toolbar)) && !empty($vocabs)) {
      $vocabulary = reset($vocabs);
      $root_term = $vocabulary->getCurrentRootTerm();
    }

    if (!empty($root_term)) {
      $values = [
          'name' => t('Initial'),
          'description' => t('Initial menu item created by install profile sxtot'),
          'vid' => $root_term->getVocabularyId(),
          'parent' => $root_term->id(),
      ];
      $menu_term = MenuTerm::create($values);
      $menu_term->save();
    }
  }

  /**
   * Returns a logger object.
   * 
   * @return \Psr\Log\LoggerInterface, default channel 'slogtx'
   */
  public static function logger($channel = 'slogtx') {
    if (!isset(self::$cacheLogger[$channel])) {
      self::$cacheLogger[$channel] = \Drupal::logger($channel);
    }
    return self::$cacheLogger[$channel];
  }

}
