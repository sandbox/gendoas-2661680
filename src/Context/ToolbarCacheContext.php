<?php

/**
 * @file
 * Contains \Drupal\slogtx\Context\ToolbarCacheContext.
 */

namespace Drupal\slogtx\Context;

use Drupal\Core\Cache\Context\CalculatedCacheContextInterface;
use Drupal\Core\Cache\CacheableMetadata;

/**
 */
class ToolbarCacheContext implements CalculatedCacheContextInterface {

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t("Toolbar's current root terms");
  }

  /**
   * {@inheritdoc}
   */
  public function getContext($root_term_ids = NULL) {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($root_term_ids = NULL) {
    return new CacheableMetadata();
  }

}

