<?php

/**
 * @file
 * Contains \Drupal\slogtx\Context\RootTermCacheContext.
 */

namespace Drupal\slogtx\Context;

use Drupal\Core\Cache\Context\CalculatedCacheContextInterface;
use Drupal\Core\Cache\CacheableMetadata;

/**
 */
class RootTermCacheContext implements CalculatedCacheContextInterface {

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t("Vocabulary's current root term");
  }

  /**
   * {@inheritdoc}
   */
  public function getContext($root_term_id = NULL) {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($root_term_id = NULL) {
    return new CacheableMetadata();
  }

}

