<?php

/**
 * @file
 * Contains \Drupal\slogtx\Event\TxEntitiesEvent.
 */

namespace Drupal\slogtx\Event;

use Symfony\Component\EventDispatcher\Event;

class TxEntitiesEvent extends Event {

  
  protected $entities;
  protected $storage;

  public function __construct(array $entities, $storage = NULL) {
    $this->entities = $entities;
    $this->storage = $storage;
  }

  public function getEntities() {
    return $this->entities;
  }

  public function getStorage() {
    return $this->storage;
  }

}
