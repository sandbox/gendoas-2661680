<?php

/**
 * @file
 * Contains \Drupal\slogtx\Event\SlogtxGenericEvent.
 */

namespace Drupal\slogtx\Event;

use Symfony\Component\EventDispatcher\Event;

class SlogtxGenericEvent extends Event {
  
  protected $data;

  public function __construct($data = NULL) {
    $this->data = $data;
  }

  public function getData() {
    return $this->data;
  }

}
