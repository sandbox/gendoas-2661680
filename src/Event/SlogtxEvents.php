<?php

/**
 * @file
 * Contains \Drupal\slogtx\Event\SlogtxEvents.
 */

namespace Drupal\slogtx\Event;

/**
 * Defines events thrown by slogtx.
 */
final class SlogtxEvents {

  /**
   * Term is just deleted
   */
  const TX_MENU_TERM_MOVED = 'slogtx.menu_term_moved';
  const TX_MENU_TERM_DELETED = 'slogtx.menu_term_deleted';
  const TX_ROOT_TERM_DELETED = 'slogtx.root_term_deleted';
  const TX_TARGET_TERM_DELETED = 'slogtx.target_term_deleted';

  const TX_TOOLBAR_CHANGED = 'slogtx.toolbar_changed';
}
