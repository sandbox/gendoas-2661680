<?php

/**
 * @file
 * Contains \Drupal\slogtx\Interfaces\TxVocabularyInterface.
 */

namespace Drupal\slogtx\Interfaces;

use Drupal\taxonomy\VocabularyInterface;
//use Drupal\slogtx\Interfaces\EntityLockInterface;

/**
 * Provides an interface defining a node type entity.
 */
interface TxVocabularyInterface extends VocabularyInterface, EntityLockInterface, TxPathLabelInterface {
  
  /**
   * Return the id of the toolbar, the vocabulary belongs to.
   * 
   * @return string
   */
  public function getToolbarId();

  /**
   * Return the name of the toolbar, the vocabulary belongs to.
   * 
   * @return string
   */
  public function getToolbarName();

  /**
   * Return the toolbar object, the vocabulary belongs to.
   * 
   * @return \Drupal\slogtx\Interfaces\TxToolbarInterface
   */
  public function getToolbar();

  /**
   * Return root term ids from root terms which belong to the vocabulary.
   * 
   * @param integer $target_tid
   *   Target term id, defaults to NULL, is set to current if NULL.
   * @param boolean $all
   *   If TRUE, return ids for all root terms, independent of status.
   * @param boolean $status
   *   if $all == FALSE, return ids with status == status.
   * @return array of root term ids
   *   An associative array with key equals values.
   */
  public function getRootTermIds($target_tid = NULL, $all = FALSE, $status = TRUE);

  /**
   * Return vocabulary objects of vocabularies which belong to the toolbar.
   * 
   * @return array of root term objects.
   *   \Drupal\slogtx\SlogRootTermInterface
   * 
   * @see self::getRootTermIds()
   */
  public function getRootTerms($all = FALSE, $status = TRUE);

}
