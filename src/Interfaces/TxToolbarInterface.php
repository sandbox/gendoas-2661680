<?php

/**
 * @file
 * Contains \Drupal\slogtx\Interfaces\TxToolbarInterface.
 */

namespace Drupal\slogtx\Interfaces;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
//use Drupal\slogtx\Interfaces\EntityLockInterface;

/**
 * Provides an interface defining a node type entity.
 */
interface TxToolbarInterface extends ConfigEntityInterface, EntityLockInterface, TxPathLabelInterface {

  /**
   * Return vocabulary ids from vocabularies which belong to the toolbar.
   * 
   * @param boolean $all
   *   If TRUE, return ids for all vocabularies, independent of status.
   * @param boolean $status
   *   if $all == FALSE, return ids with status == status.
   * @return array of vocabulary ids
   */
  public function getVocabularyIds($all = FALSE, $status = TRUE);

  /**
   * Return vocabulary objects of vocabularies which belong to the toolbar.
   * 
   * @return array of vocabulary objects.
   *   \Drupal\slogtx\Interfaces\TxVocabularyInterface
   * 
   * @see self::getVocabularyIds()
   */
  public function getVocabularies($all = FALSE, $status = TRUE);

}
