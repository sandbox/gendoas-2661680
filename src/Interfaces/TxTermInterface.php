<?php

/**
 * @file
 * Contains \Drupal\slogtx\Interfaces\TxTermInterface.
 */

namespace Drupal\slogtx\Interfaces;

use Drupal\taxonomy\TermInterface;

/**
 * Provides an interface defining a node type entity.
 */
interface TxTermInterface extends TermInterface, TxPathLabelInterface {
  
  /**
   * Return the parent id of the term.
   * 
   * @return integer
   */
  public function getParentID();

  /**
   * Set the parent id of the term.
   * 
   * Logs an error on rootterm/term missmatch 
   * - for slogtx_rt object value has to be 0
   * - for slogtx_mt object value has to be greater 0
   * 
   * @return self
   */
  public function setParentID($value);

  /**
   * Return the vocabulary object the term belongs to.
   * 
   * @return \Drupal\slogtx\Entity\TxVocabulary
   */
  public function getVocabulary();

  /**
   * Return the target term id of the term this rootterm/term belongs to.
   * 
   * Target term is a term without a parent term.
   * For example for node target entity the target term's name may be Node.123,
   * or for role target entity the target term's name may be UserRole.12
   * 
   * @return integer or FALSE
   */
  public function getTargetTermID();

  /**
   * Return the target term object this rootterm/term belongs to.
   * 
   * Target term is a term without a parent term.
   * For example for node target entity the target term's name may be Node.123,
   * or for role target entity the target term's name may be UserRole.12
   * 
   * @return \Drupal\slogtx\Entity\TargetTerm or FALSE
   */
  public function getTargetTerm();

  /**
   * Return the root term id of the term this term belongs to.
   * 
   * Root term is the term with only the target term as parent term.
   * It ist the parent for hierarchical menu terms.
   * 
   * @return integer or FALSE
   */
  public function getRootTermID();

  /**
   * Return the root term object this term belongs to.
   * 
   * Root term is the term with only the target term as parent term.
   * It ist the parent for hierarchical menu terms.
   * 
   * @return \Drupal\slogtx\Entity\RootTerm or FALSE
   */
  public function getRootTerm();

  /**
   * Return the entity storage depending on the term type.
   * 
   * @param string $type_id
   *  The term entity's type id.
   * @return \Drupal\taxonomy\TermStorageInterface
   */
  public function getStorage($type_id);
  
  /**
   * Return the storage of child terms.
   */
  public function getChildStorage();

  /**
   * Return the first child term of the term.
   */
  public function getFirstChildTid();

  /**
   * Whether the term  has any children at all.
   */
  public function hasChildren($reset = FALSE);
  
}
