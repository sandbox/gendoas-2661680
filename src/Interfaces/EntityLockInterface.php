<?php

/**
 * @file
 * Contains \Drupal\slogtx\Interfaces\EntityLockInterface.
 */

namespace Drupal\slogtx\Interfaces;

/**
 * Provides an interface defining a node type entity.
 */
interface EntityLockInterface {

  /**
   * Determines whether the entity is locked for delete/update.
   *
   * @param string $operation ('delete' or 'update')
   * @return string|false
   *   The module name that locks the entity or FALSE.
   */
  public function isLocked($operation);
  
  /**
   * Lock the entity for delete/update.
   * 
   * @param string $operation ('delete' or 'update')
   * @param string $module 
   *   Name of the locking module. 
   */
  public function lock($operation, $module);
  
}
