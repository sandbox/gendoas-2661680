<?php

/**
 * @file
 * Contains \Drupal\slogtx\Interfaces\TxPathLabelInterface.
 */

namespace Drupal\slogtx\Interfaces;

/**
 * Provides an interface defining entity with path label.
 */
interface TxPathLabelInterface {

  /**
   * Return the path label of this entity.
   * 
   * The path label contains toolbar label for toolbar entity,
   * toolbar/vocabulary labels for vocabulary entity,
   * toolbar/vocabulary/targetterm labels for target term entity,
   * toolbar/vocabulary/targetterm/rootterm labels for root term entity,
   * and for menu term entity additionally the menuterm label.
   * 
   * @return string
   */
  public function pathLabel();
  

}
