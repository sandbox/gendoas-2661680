<?php

/**
 * @file
 * Contains \Drupal\slogtx\TargetEntityPluginManager.
 */

namespace Drupal\slogtx;

use Drupal\slogtx\SlogTx;
use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages target entity plugins.
 * 
 * Target entity plugins are used by slog vocabularies.
 *
 * @see \Drupal\slogtx\Annotation\SlogtxTargetEntity
 * @see \Drupal\slogtx\Plugin\slogtx\TargetEntityInterface
 * @see \Drupal\slogtx\Plugin\slogtx\TargetEntityBase
 */
class TargetEntityPluginManager extends DefaultPluginManager implements FallbackPluginManagerInterface {

  /**
   * Caches the settings of target entity plugins.
   *
   * @var array of target entity plugin settings 
   */
  protected $settings;

  /**
   * For caching created plugins
   *
   * @var array
   */
  protected $cached_all_plugins;

  /**
   * Constructs a TargetEntityPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/slogtx/TargetEntity', $namespaces, $module_handler, // 
        'Drupal\slogtx\Plugin\slogtx\TargetEntityInterface', //
        'Drupal\slogtx\Annotation\SlogtxTargetEntity');
    $this->setCacheBackend($cache_backend, 'slogtx.target_entity_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return SlogTx::TARGETENTITY_ID_NONE;
  }

  /**
   * Stores the settings of target entity plugins.
   * 
   * Settings are stored as state in the key_value table,
   *   with name=slogtx.settings.target_entity_plugins.
   * Settings for all target entity plugins are stored under this key name.
   * 
   * @see self::getSettingsKey()
   * 
   * @param array $plugin_settings
   *  The settings for an individual plugin
   * @param string $plugin_id
   */
  public function setSettings(array $plugin_settings, $plugin_id) {
    $settings_key = $this->getSettingsKey();
//todo::review:: Drupal::state() - TargetEntityXXX
    $settings = \Drupal::state()->get($settings_key);
    $settings[$plugin_id] = $settings[$plugin_id] ?: [];
    $settings[$plugin_id] = array_merge($settings[$plugin_id], $plugin_settings);

    // store the state in db
    \Drupal::state()->set($settings_key, $settings);

    // refresh cached settings
    $this->settings = $settings;
  }

  /**
   * Retrieves the settings of target entity plugins.
   * 
   * @see self::setSettings()
   * 
   * @return array
   */
  public function getSettings() {
    if (!isset($this->settings)) {
      $settings_key = $this->getSettingsKey();
      $this->settings = \Drupal::state()->get($settings_key);
      $this->settings = isset($this->settings) ? $this->settings : [];
    }

    return $this->settings;
  }

  /**
   * Deletes the settings of all target entity plugins.
   * 
   * This is deleting one db record, since all settings are saved in one record.
   * 
   * @see self::setSettings()
   */
  public function deleteAllSettings() {
//todo::now - Drupal::state()->delete
    \Drupal::state()->delete($this->getSettingsKey());
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    if ($definition = $this->getDefinition($plugin_id, FALSE)) {
      // get the configuration from state (key_value table)
      $configuration += $definition;
      if ($configuration) {
        $settings = $this->getSettings();
        if (isset($settings[$plugin_id])) {
          $configuration['settings'] = $settings[$plugin_id];
          $configuration = array_merge($configuration, $settings[$plugin_id]);
        }

        return parent::createInstance($plugin_id, $configuration);
      }
    }

    return NULL; // invalid 
  }

  /**
   * Returns the key (field 'name') where the settings are stored.
   * 
   * The settings are stored as state in the key_value table.
   * 
   * @return string
   */
  protected function getSettingsKey() {
    return 'slogtx.settings.target_entity_plugins';
  }

  /**
   * Returns the ids of all target entity plugins.
   * 
   * @return array of ids
   */
  public function getAllTargetEntityIds() {
    $definitions = $this->getDefinitions();
    return array_keys($definitions);
  }

  /**
   * Return target entity plugin objects.
   * 
   * Default/Fallback first, the other sorted by key.
   * 
   * @param boolean $all
   *   If TRUE, return plugin objects, independent of status.
   * @param boolean $status
   *   if $all == FALSE, return plugin objects, with the specified status.
   * @return array of target entitys plugin objects.
   *   \Drupal\slogtx\Plugin\slogtx\TargetEntityInterface
   */
  public function getTargetEntityPlugins($all = FALSE, $status = TRUE) {
    if (!$this->cached_all_plugins) {
      $fallback_id = $this->getFallbackPluginId(NULL);
      $fallback_plugin = FALSE;
      $all_plugins = $plugins_tmp = $top_plugins = [];
      $ids = $this->getAllTargetEntityIds();
      $configuration = ['no_vocabulary' => TRUE];
      foreach ($ids as $plugin_id) {
        if ($instance = $this->createInstance($plugin_id, $configuration)) {
          if ($plugin_id === $fallback_id) {
            $fallback_plugin = $instance;
          }
          else {
            $all_plugins[$plugin_id] = $instance;
            $title = (string) $instance->title;
            $plugins_tmp[$title] = $plugin_id;
          }
        }
      }

      // fallback on top
      $this->cached_all_plugins = $fallback_plugin //
          ? [$fallback_id => $fallback_plugin] : [];
      // sort by title
      ksort($plugins_tmp);
      foreach ($plugins_tmp as $id) {
        $this->cached_all_plugins[$id] = $all_plugins[$id];
      }
    }

    if ($all) {
      $plugins = $this->cached_all_plugins;
    }
    else {
      $plugins = [];
      foreach ($this->cached_all_plugins as $plugin_id => $plugin) {
        if ($plugin->isValid() && (boolean) $plugin->status) {
          $plugins[$plugin_id] = $plugin;
        }
      }
    }

    return $plugins;
  }

  /**
   * 
   * @param string $plugin_id
   * @return \Drupal\slogtx\Plugin\slogtx\TargetEntityInterface || NULL
   */
  public function getTargetEntityPlugin($plugin_id) {
    $plugin = NULL;
    if ($plugins = $this->getTargetEntityPlugins(TRUE)) {
      if ($plugins[$plugin_id] && $plugins[$plugin_id]->isValid()) {
        $plugin = $plugins[$plugin_id];
      }
    }
    return $plugin;
  }

}
