<?php

/**
 * @file
 * Contains \Drupal\slogtx\TxEntityStorageTrait.
 */

namespace Drupal\slogtx;

use Drupal\slogtx\SlogTx;

trait TxEntityStorageTrait {

  /**
   * {@inheritdoc}
   */
  public function getStorage($type_id = NULL) {
    if (!isset($type_id)) {
      $type_id = $this->getEntityTypeId();
    }
    return SlogTx::entityStorage($type_id);
  }

}
