<?php

/**
 * @file
 * Contains \Drupal\slogtx\Entity\RootTerm.
 */

namespace Drupal\slogtx\Entity;

use Drupal\slogtx\SlogTx;
use Drupal\slogtx\Entity\MenuTerm;
use Drupal\slogtx\TxEntityLockTrait;
use Drupal\Core\Cache\Cache;

/**
 * Defines the slog root term entity.
 *
 * For more definitions see slogtx_ui_entity_type_build().
 *
 * @ContentEntityType(
 *   id = "slogtx_rt",
 *   label = @Translation("Root term"),
 *   bundle_label = @Translation("Slog vocabulary"),
 *   handlers = {
 *     "access" = "Drupal\slogtx\Access\SlogRootTermAccessControlHandler",
 *     "storage" = "Drupal\slogtx\Storage\RootTermStorage",
 *     "storage_schema" = "Drupal\taxonomy\TermStorageSchema",
 *     "view_builder" = "Drupal\taxonomy\TermViewBuilder",
 *     "views_data" = "Drupal\taxonomy\TermViewsData",
 *     "form" = {
 *       "default" = "Drupal\slogtx_ui\Form\RootTermForm",
 *       "delete" = "Drupal\slogtx_ui\Form\RootTermDeleteForm",
 *       "enable" = "Drupal\slogtx_ui\Form\RootTermEnableForm",
 *       "disable" = "Drupal\slogtx_ui\Form\RootTermEnableForm"
 *     },
 *     "translation" = "Drupal\taxonomy\TermTranslationHandler"
 *   },
 *   base_table = "taxonomy_term_data",
 *   data_table = "taxonomy_term_field_data",
 *   revision_table = "taxonomy_term_revision",
 *   revision_data_table = "taxonomy_term_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer slog taxonomy",
 *   entity_keys = {
 *     "id" = "tid",
 *     "revision" = "revision_id",
 *     "bundle" = "vid",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   bundle_entity_type = "slogtx_voc",
 *   common_reference_target = FALSE,
 *   additional = {
 *     "slog_is_lockable" = { "delete", "update", "disable", "preserve" },
 *   }
 * )
 * 
 *     "status" = "status",
 * 
 */
//todo::cleanup
class RootTerm extends TxTermBase {

  use TxEntityLockTrait;

  /**
   * Cache once retrieved value.
   *
   * @var boolean 
   */
  protected $is_valid_root_term = NULL;

  /**
   * {@inheritdoc}
   * 
   * Overrides Drupal\slogtx\Entity\TxTermBase::isRootTerm()
   */
  public function isRootTerm() {
    return TRUE;
  }

  public function isVisible() {
    return $this->isPublished() && $this->getTargetTerm()->isVisible();
  }

  public function getSiblingRootTerm($rt_name, $do_create = TRUE) {
    if ($rt_name === SlogTx::ROOTTERM_DEFAULT) {
      return $this;
    }

    $rootterm = FALSE;
    $vocabulary = $this->getVocabulary();
    $target_term = $this->getTargetTerm();
    $target_tid = $target_term->id();
    if ($target_term->hasRootTermByName($rt_name)) {
      $rootterm = $vocabulary->getRootTermByName($rt_name, $target_tid);
    }
    elseif ($do_create) {
      $values = [
        'name' => $rt_name,
        'description' => t('Generated sibling root term.'),
      ];
      $target_term->addRootTerm($values);
      $rootterm = $vocabulary->getRootTermByName($rt_name, $target_tid);
    }

    return ($rootterm ?: FALSE);
  }

  /**
   * //todo::text - hasMenuTermByName() ...
   * 
   * @param string $name
   * @param integer $parent_id optional
   * @return boolean
   */
  public function hasMenuTermByName(string $name, $parent_id = NULL, $reset = FALSE) {
    $vid = $this->getVocabularyId();
    $parent_id = $parent_id ?? $this->id();
    return $this->getChildStorage()->hasMenuTermByName($vid, $parent_id, $name, $reset);
  }

  public function getMenuTermByName(string $name, $parent_id = NULL, $reset = FALSE) {
    if ($this->hasMenuTermByName($name, $parent_id, $reset)) {
      $vid = $this->getVocabularyId();
      $parent_id = $parent_id ?? $this->id();
      $menu_tid = $this->getChildStorage()->getMenuTermIdByName($vid, $parent_id, $name);
      return SlogTx::getMenuTerm($menu_tid);
    }

    return NULL;
  }

  public function createMenuTermByName(string $name, $description, $parent_id = NULL) {
    $parent_id = $parent_id ?? $this->id();
    $values = [
      'name' => $name,
      'description' => $description,
      'vid' => $this->getVocabularyId(),
      'parent' => $parent_id,
    ];
    MenuTerm::create($values)->save();

    return $this->getMenuTermByName($name, $parent_id, TRUE);
  }

  /**
   * {@inheritdoc}
   * 
   * Implements Drupal\slogtx\Entity\TxTermBase::isValidTermByParents()
   */
  public function isValidTermByParents() {
    if (!isset($this->is_valid_root_term)) {
      $target_term = $this->getTargetTerm();
      $this->is_valid_root_term = ($target_term && $target_term->isValidTermByParents());
    }

    return $this->is_valid_root_term;
  }

  /**
   * {@inheritdoc}
   * 
   * Overrides \Drupal\slogtx\Entity\TxTermBase::getUpperObject()
   */
  public function getUpperObject() {
    return $this->getTargetTerm();
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetTerm() {
    if (!isset($this->target_term)) {
      $this->target_term = $this->getStorage('slogtx_tt')->load($this->getParentID());
    }
    return $this->target_term;
  }

  /**
   * {@inheritdoc}
   * 
   * Overrides \Drupal\slogtx\Entity\TxTermBase::deleteLocks()
   */
  public function deleteLocks() {
    // with blocker==NULL the locks will be deleted
    $this->lock(self::getAllAllowedOperations(), NULL);
  }

  /**
   * Add cache context to this root term entity.
   * 
   * @see service "cache_context.slogtx_rtc"
   * 
   * @return \Drupal\slogtx\Entity\RootTerm
   */
  public function addRootTermCaches() {
    $this->addCacheContexts(['slogtx_rtc:' . $this->id()])
        ->addCacheTags(['slogtx_rt:' . $this->id()]);
    return $this;
  }

  /**
   * Invalidate the cache by invalidating the tag for this entity.
   */
  public function invalidateCache() {
    Cache::invalidateTags(['slogtx_rt:' . $this->id()]);
  }

}
