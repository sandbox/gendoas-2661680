<?php

/**
 * @file
 * Contains \Drupal\slogtx\Entity\TargetTerm.
 */

namespace Drupal\slogtx\Entity;

use Drupal\slogtx\SlogTx;
use Drupal\slogtx\Entity\RootTerm;
use Drupal\taxonomy\TermInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\slogtx\TxEntityLockTrait;

/**
 * Defines the slog root term entity.
 *
 * For more definitions see slogtx_ui_entity_type_build().
 *
 * @ContentEntityType(
 *   id = "slogtx_tt",
 *   label = @Translation("Target term"),
 *   bundle_label = @Translation("Slog vocabulary"),
 *   handlers = {
 *     "access" = "Drupal\slogtx\Access\SlogRootTermAccessControlHandler",
 *     "storage" = "Drupal\slogtx\Storage\TargetTermStorage",
 *     "storage_schema" = "Drupal\taxonomy\TermStorageSchema",
 *     "translation" = "Drupal\taxonomy\TermTranslationHandler"
 *   },
 *   base_table = "taxonomy_term_data",
 *   data_table = "taxonomy_term_field_data",
 *   revision_table = "taxonomy_term_revision",
 *   revision_data_table = "taxonomy_term_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer slog taxonomy",
 *   entity_keys = {
 *     "id" = "tid",
 *     "revision" = "revision_id",
 *     "bundle" = "vid",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   bundle_entity_type = "slogtx_voc",
 *   common_reference_target = FALSE,
 *   additional = {
 *     "slog_is_lockable" = { "delete", "update", "disable" },
 *   }
 * )
 * 
 *     "status" = "status",
 */
//todo::cleanup
class TargetTerm extends TxTermBase {

  use TxEntityLockTrait;

  /**
   * {@inheritdoc}
   * 
   * Overrides Drupal\slogtx\Entity\TxTermBase::isTargetTerm()
   */
  public function isTargetTerm() {
    return TRUE;
  }

  public function isVisible() {
    return $this->isPublished() && $this->getVocabulary()->isEnabled();
  }

  /**
   * {@inheritdoc}
   * 
   * Implements Drupal\slogtx\Entity\TxTermBase::isValidTermByParents()
   */
  public function isValidTermByParents() {
    return ($this->getParentID() == 0);
  }

  /**
   * {@inheritdoc}
   * 
   * Overrides \Drupal\slogtx\Entity\MenuTerm::getUpperObject()
   */
  public function getUpperObject() {
    return $this->getVocabulary();
  }

  /**
   * {@inheritdoc}
   */
  public function getRootTermIds($all = FALSE, $status = TRUE) {
    $storage = $this->getChildStorage();
    $ids = $storage->getRootTermIds($this->getVocabularyId(), $this->id());
    if (!$all) {
      $terms = $storage->loadMultiple($ids);
      $ids = [];
      foreach ($terms as $tid => $term) {
        if ($term->getStatus() == $status) {
          $ids[$tid] = $tid;
        }
      }
    }

    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function getRootTerms($all = FALSE, $status = TRUE) {
    return $this->getChildStorage()->loadMultiple($this->getRootTermIds($all, $status));
  }

  /**
   * {@inheritdoc}
   */
  public function hasRootTermByName($name) {
    $vid = $this->getVocabularyId();
    return $this->getChildStorage()->hasRootTermByName($vid, $this->id(), $name);
  }

  /**
   * {@inheritdoc}
   * 
   * Overrides \Drupal\Core\Entity\ContentEntityInterface::preSave()
   */
  public function preSave(EntityStorageInterface $storage) {
    // Ensure parent for root term is 0
    $this->setParentID(0);
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    if (!$update) {
      // lock if set
      if ($this->locked) {
        $this->lock($this->locked, 'slogtx');
      }

      // add required root terms if any
      $this->addRequiredRootTerms();
    }
  }

  /**
   * Add field status for slog root terms.
   * 
   */
  private function addRequiredRootTerms() {
    $vid = $this->getVocabularyId();
    $required = $this->getRequiredRootTerms();
    foreach ($required as $name => $data) {
      $values = [
          'name' => $name,
          'description' => $data['description'],
      ];
      $root_term = $this->addRootTerm($values);

      // lock the root term (delete, update)
      if (!empty($data['locked'])) {
        $root_term->lock($data['locked'], $data['provider']);
      }
    }
  }

  public function addRootTerm($values) {
    $values += [
        'vid' => $this->getVocabularyId(),
        'parent' => $this->id(),
        'langcode' => $this->langcode,
    ];
    $root_term = RootTerm::create($values);
    $root_term->save();
    return $root_term;
  }

  private function getRequiredRootTerms() {
    $required = [];
    $required[SlogTx::ROOTTERM_DEFAULT] = [
        'description' => t('The Default root term: is required, not deletable, not to disable and name not changeable.'),
        'locked' => ['delete', 'disable', 'preserve'],
        'provider' => 'slogtx',
    ];
    return $required;
  }

  /**
   * {@inheritdoc}
   * 
   * Overrides \Drupal\slogtx\Entity\TxTermBase::deleteLocks()
   */
  public function deleteLocks() {
    $vid = $this->getVocabularyId();
    $target_term_id = $this->id();
    $pattern = "slogtx.locked.term.{$vid}.{$target_term_id}.%";
    $keys = SlogTx::dbGetLockedKeys($pattern);
    SlogTx::getLockedStorage()->deleteMultiple($keys);
  }

  /**
   * Invalidate cache for all root terms if the target term.
   */
  public function invalidateRootTermCache() {
    foreach ($this->getRootTerms(TRUE) as $root_term) {
      $root_term->invalidateCache();
    }
  }

}
