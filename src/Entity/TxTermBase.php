<?php

/**
 * @file
 * Contains \Drupal\slogtx\Entity\TxTermBase.
 */

namespace Drupal\slogtx\Entity;

use Drupal\slogtx\SlogTx;
use Drupal\slogtx\Interfaces\TxTermInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\slogtx\TxEntityStorageTrait;
use Drupal\slogtx\Event\SlogtxEvents;
use Drupal\slogtx\Event\TxEntitiesEvent;

/**
 * Defines an abstract base class for slogtx term entities.
 * 
 * There are three slogtx term entity types:
 *  - \Drupal\slogtx\Entity\TargetTerm
 *  - \Drupal\slogtx\Entity\RootTerm
 *  - \Drupal\slogtx\Entity\MenuTerm
 */
abstract class TxTermBase extends Term implements TxTermInterface {

  use TxEntityStorageTrait;

  /**
   * Cache once loaded vocabulary object.
   *
   * @var \Drupal\slogtx\Entity\TxVocabulary 
   */
  protected $vocabulary;
  protected $target_term;
  protected $root_term;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type, $bundle = FALSE, $translations = []) {
    parent::__construct($values, $entity_type, $bundle, $translations);
    if (!defined('MAINTENANCE_MODE')) {
      SlogTx::setEntityInstance($entity_type, $this->id(), $this);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getChildTypeId() {
    return $this->isTargetTerm() ? 'slogtx_rt' : 'slogtx_mt';
  }

  /**
   * {@inheritdoc}
   */
  public function getChildStorage() {
    return $this->getStorage($this->getChildTypeId());
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstChildTid() {
    return $this->getChildStorage()->getFirstChildTid($this->id());
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstChildTerm() {
    return SlogTx::entityInstance($this->getChildTypeId(), $this->getFirstChildTid());
  }

  /**
   * {@inheritdoc}
   */
  public function hasChildren($reset = FALSE) {
    return $this->getChildStorage()->hasChildren($this->id(), $reset);
  }

  /**
   * Overrides Drupal\Core\Entity\Entity::save()
   */
  public function save() {
    if ($this->isValidTerm()) {
      if ($this->isMenuTerm()) {
        $this->setPublished();
      }
      return parent::save();
    }

    // log error for invalid term
    $args = [
        '@type' => $this->getEntityType()->getLabel(),
        '%term' => $this->id(),
    ];
    $msg = t('Aborted saving an unvalid @type: %term.', $args);
    \Drupal::messenger()->addError($msg);
    SlogTx::logger()->error($msg);
  }

  /**
   * {@inheritdoc}
   * 
   *  - Invalidate cache, which depends on the root term.
   *  - Save term's status as state in key_value table.
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    if (!$this->isTargetTerm()) {
      $this->invalidateRootTermCache();
    }
  }

  /**
   * {@inheritdoc}
   * 
   *  - Invalidate cache, which depends on the root term.
   *  - Delete locks.
   *  - Delete status.
   *  - Delete child terms
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    foreach ($entities as $term) {
      if (!$term->isMenuTerm()) {
        $term->invalidateRootTermCache();
        $term->deleteLocks();

        $child_storage = $term->getChildStorage();
        if ($children = $child_storage->getChildren($term)) {
          $event_done = FALSE;
          foreach ($children as $child) {
            if (!$event_done && $child->isMenuTerm()) {
              $event_done = TRUE;
              $dispatcher = \Drupal::service('event_dispatcher');
              $event = new TxEntitiesEvent($children, $storage);
              $dispatcher->dispatch(SlogtxEvents::TX_MENU_TERM_DELETED, $event);
            }
            
            $child->delete();
          }
        }
      }
    }

    if (!$term->isMenuTerm()) {
      $dispatcher = \Drupal::service('event_dispatcher');
      $event = new TxEntitiesEvent($entities, $storage);
      if ($term->isRootTerm()) {
        $dispatcher->dispatch(SlogtxEvents::TX_ROOT_TERM_DELETED, $event);
      }
      elseif ($term->isTargetTerm()) {
        $dispatcher->dispatch(SlogtxEvents::TX_TARGET_TERM_DELETED, $event);
      }
    }
  }

  /**
   * Return the upper object in the hierarchy.
   * 
   * There are three cases:
   *  - Menu term: root term is the upper object
   *  - Root term: target term is the upper object
   *  - Target term: vocabulary is the upper object
   * 
   * @return object
   */
  abstract public function getUpperObject();

  /**
   * Validate by parents.
   * 
   * There are three cases:
   * - Target term: parent tid==0.
   * - Root term: parent tid>0 and parent is target term.
   * - Menu term: parent tid>0 and parent is NOT target term.
   */
  abstract public function isValidTermByParents();

  /**
   * {@inheritdoc}
   */
  public function hasStatus() {
    return !$this->isMenuTerm();
  }
  
  /**
   * {@inheritdoc}
   */
  public function isLockedDisable() {
    return $this->isOperationLocked('disable');
  }

  /**
   * {@inheritdoc}
   */
  public function isLockedDelete() {
    return $this->isOperationLocked('delete');
  }

  /**
   * {@inheritdoc}
   */
  public function isOperationLocked($operation) {
    if ($this->isMenuTerm() || in_array($this->label(), SlogTx::getLockedLabels($this->getEntityTypeId()))) {
      return TRUE;
    }
    return $this->isLocked($operation);
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->isMenuTerm() ? TRUE : $this->isPublished();
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($published) {
    if ($this->hasStatus()) {
      return $published ? $this->setPublished() : $this->setUnpublished();
    }
    return $this;
  }

  public function getTargetTermIDLocal() {
    return $this->isTargetTerm() ? $this->id() : $this->getParentID();
  }

  /**
   * {@inheritdoc}
   */
  protected function getTermLockedKey($operation) {
    if ($this->isLockable()) {
      $vid = $this->getVocabularyId();
      $target_term_id = $this->getTargetTermIDLocal();
      return "slogtx.locked.term.{$vid}.{$target_term_id}.{$operation}";
    }
  }

  /**
   * Delete the term's locks.
   */
  public function deleteLocks() {
    // By default do nothing
    // Override this function for overriding locks
  }

  /**
   * {@inheritdoc}
   */
  public function getParentID($no_msg = FALSE) {
    $parent_id = $this->get('parent')->target_id;
    if (empty($parent_id) && !$this->isNew() && !$this->isTargetTerm() && !$no_msg) {
      $args = [
          '@type' => $this->getEntityType()->getLabel(),
          '%term' => $this->id(),
      ];
      $msg = t('Term parent missing for @type: %term.', $args);
      SlogTx::logger()->error($msg);
    }
    return $parent_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentID($value) {
    return $this->set('parent', $value);
  }

  /**
   * {@inheritdoc}
   */
  public function setVocabularyID($value) {
    if ($value !== $this->getVocabularyId()) {
      unset($this->vocabulary);
      // sync vid and bundle entityKey
      $this->entityKeys['bundle'] = $value;
    }
    return $this->set('vid', $value);
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabularyId() {
    // !!!!!  ->bundle() is calling ->getEntityKey('bundle')  !!!!!
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabulary() {
    if (!isset($this->vocabulary)) {
      $this->vocabulary = SlogTx::getVocabulary($this->getVocabularyId());
    }

    return $this->vocabulary;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($value) {
    return $this->set('weight', $value);
  }

  /**
   * Whether this is a target term object.
   * 
   * @return boolean
   *  TRUE if term is a target term
   */
  public function isTargetTerm() {
    return FALSE;
  }

  /**
   * Whether this is a root term object.
   * 
   * @return boolean
   *  TRUE if term is a root term
   */
  public function isRootTerm() {
    return FALSE;
  }

  /**
   * Whether this is a menu term object.
   * 
   * @return boolean
   *  TRUE if term is a menu term
   */
  public function isMenuTerm() {
    return FALSE;
  }

  /**
   * Whether term object is valid.
   * 
   * - TRUE if is new entity
   * - TRUE id parent id is set
   * - TRUE if valid by parents
   * 
   * @return boolean
   *  TRUE if term is a valid term
   */
  public function isValidTerm() {
    if ($this->isNew()) {
      return TRUE;
    }

    $parent_id = $this->getParentID();
    if (!isset($parent_id)) {
      return FALSE;
    }

    // ask subclass
    return $this->isValidTermByParents();
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetTermID() {
    return $this->getTargetTerm()->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetTerm() {
    throw new \LogicException('Invalid function call: getTargetTerm()');
  }

  public function setRequiredEntity() {
    $entity = NULL;
    $vocabulary = $this->getVocabulary();
    $te_plugin = $vocabulary->getTargetEntityPlugin();
    $toolbar_id = $vocabulary->getToolbarId();
    $set_entity = (!$te_plugin->isEntityFixed() || $toolbar_id === 'role');
    if ($te_plugin->isEntityRequired() && $set_entity) {
      $tt_name = $this->getTargetTerm()->label();
      if ($entity = $te_plugin->getEntityFromTargetTermName($tt_name)) {
        $te_plugin->setEntity($entity);
      }
    }
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getRootTermID() {
    return $this->getRootTerm()->id();
  }

  /**
   * {@inheritdoc}
   * 
   * Returns FALSE if $this is target term or root term object.
   */
  public function getRootTerm() {
    throw new \LogicException('Invalid function call: getRootTerm()');
  }

  /**
   * {@inheritdoc}
   */
  public function pathLabel() {
    if ($upper = $this->getUpperObject()) {
      $path = [$upper->pathLabel(), $this->label()];
      return implode('/', $path);
    }

    // upper not found, shouldn't happen
    $args = ['@term' => $this->id() . '.' . $this->label()];
    SlogTx::logger()->warning('TxTermBase::pathLabel(): Upper object not found: @term.', $args);

    return $this->label();
  }

  public function getSettableEntity() {
    $vocabulary = $this->getVocabulary();
    $te_plugin = $vocabulary->getTargetEntityPlugin();
    if ($te_plugin->isEntitySettable()) {
      $term_name = $this->isTargetTerm() ? $this->label() : $this->getTargetTerm()->label();
      if (!$entity = $vocabulary->getEntityFromTargetTermName($term_name)) {
        $args = [
            '@entitytype' => $te_plugin->id(),
            '@term_name' => $term_name,
        ];
        $message = t('Entity not found: @entitytype / @term_name', $args);
        throw new \LogicException($message);
      }

      return $entity;
    }

    return NULL;
  }

  /**
   * Whether the object is visible.
   * 
   * Override this for root term and target term if disabled.
   * 
   * @return boolean
   */
  public function isVisible() {
    return TRUE;
  }

  public function invalidateRootTermCache() {
    // do nothing by default
    // this is for menu terms only
  }

}
