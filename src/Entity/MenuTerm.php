<?php

/**
 * @file
 * Contains \Drupal\slogtx\Entity\MenuTerm.
 */

namespace Drupal\slogtx\Entity;

use Drupal\slogtx\SlogTx;
use Drupal\slogtx\Entity\RootTerm;
use Drupal\slogtx\Event\SlogtxEvents;
use Drupal\slogtx\Event\SlogtxGenericEvent;

/**
 * Defines the taxonomy term entity.
 *
 * For more definitions see slogtx_ui_entity_type_build().
 *
 * @ContentEntityType(
 *   id = "slogtx_mt",
 *   label = @Translation("Menu term"),
 *   bundle_label = @Translation("Root term"),
 *   handlers = {
 *     "storage" = "Drupal\slogtx\Storage\MenuTermStorage",
 *     "storage_schema" = "Drupal\taxonomy\TermStorageSchema",
 *     "view_builder" = "Drupal\taxonomy\TermViewBuilder",
 *     "access" = "Drupal\taxonomy\TermAccessControlHandler",
 *     "views_data" = "Drupal\taxonomy\TermViewsData",
 *     "translation" = "Drupal\taxonomy\TermTranslationHandler"
 *   },
 *   base_table = "taxonomy_term_data",
 *   data_table = "taxonomy_term_field_data",
 *   revision_table = "taxonomy_term_revision",
 *   revision_data_table = "taxonomy_term_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer slog taxonomy",
 *   entity_keys = {
 *     "id" = "tid",
 *     "revision" = "revision_id",
 *     "bundle" = "vid",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   bundle_entity_type = "slogtx_voc",
 *   common_reference_target = FALSE,
 * )
 */
//todo::cleanup
class MenuTerm extends TxTermBase {

  /**
   * Cache once retrieved value.
   *
   * @var boolean 
   */
  protected $is_valid_menu_term = NULL;

  /**
   * {@inheritdoc}
   * 
   * Overrides Drupal\slogtx\Entity\TxTermBase::isMenuTerm()
   */
  public function isMenuTerm() {
    return TRUE;
  }

  public function isBaseMenuTerm() {
    return ($this->getParentID() == $this->getRootTerm()->id());
  }

  /**
   * Return Menu term if exists, create on creation request.
   * 
   * @param string $menu_label
   *  Search menu term by term label
   * @param boolean $do_create
   *  Create if not exists
   * @param string $description optional
   *  Description for creating term
   * @return mixed
   *  \Drupal\slogtx\Entity\MenuTerm or FALSE
   */
  public function getChildTermByName($menu_label, $do_create = FALSE, $description = '') {
    $base_tid = $this->id();
    $root_term = $this->getRootTerm();
    $has_child = $root_term->hasMenuTermByName($menu_label, $base_tid);
    if (!$has_child && $do_create) {
      $values = [
          'vid' => $this->getVocabularyId(),
          'name' => (string) $menu_label,
          'description' => (string) $description,
          'parent' => $base_tid,
          'weight' => REQUEST_TIME,
      ];
      static::create($values)->save();
      $has_child = $root_term->hasMenuTermByName($menu_label, $base_tid, TRUE);
    }

    if ($has_child) {
      return $root_term->getMenuTermByName($menu_label, $base_tid);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   * 
   * Return the root term object of the term.
   */
  public function getUpperObject() {
    return $this->getRootTerm();
  }

  /**
   * {@inheritdoc}
   */
  public function getRootTerm() {
    if (!isset($this->root_term)) {
      $parents = $this->getStorage('slogtx_mt')->loadAllParents($this->id());
      $term = array_pop($parents);
      $this->root_term = $this->getStorage('slogtx_rt')->load($term->id());
    }
    return $this->root_term;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetTerm() {
    if (!isset($this->target_term)) {
      $this->target_term = $this->getRootTerm()->getTargetTerm();
    }
    return $this->target_term;
  }

  /**
   * {@inheritdoc}
   */
  public function pathLabel() {
    $path = [$this->getRootTerm()->pathLabel(), $this->label()];
    return implode('/', $path);
  }

  public function menuPathLabel($self) {
    $path = $self ? [$this->label()] : [];
    $root_term_id = $this->getRootTermID();
    $parent_id = $this->getParentID();
    if ($root_term_id && $root_term_id != $parent_id) {
      $menu_term = SlogTx::getMenuTerm($parent_id);
      array_unshift($path, $menu_term->label());
      $parent_id = SlogTx::getMenuTerm($parent_id)->getParentID();
      if ($root_term_id != $parent_id) {
        $menu_term = SlogTx::getMenuTerm($parent_id);
        array_unshift($path, $menu_term->label());
      }
    }
    return implode('; ', $path);
  }

  /**
   * Whether the target menu term is on the max depth position.
   * 
   * @return boolean
   */
  public function isPositionMaxDepth() {
    $root_term_id = $this->getRootTermID();
    $parent_id = $this->getParentID();
    if ($root_term_id && $root_term_id != $parent_id) {
      $parent_id = SlogTx::getMenuTerm($parent_id)->getParentID();
      if ($root_term_id != $parent_id) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   * 
   * Implements Drupal\slogtx\Entity\TxTermBase::isValidTermByParents()
   */
  public function isValidTermByParents() {
    if (!isset($this->is_valid_menu_term)) {
      $rootterm = $this->getRootTerm();
      $this->is_valid_menu_term = ($rootterm && $rootterm->isValidTermByParents());
    }

    return $this->is_valid_menu_term;
  }

  /**
   * 
   */
  public function invalidateRootTermCache() {
    if ($root_term = $this->getRootTerm()) {
      // invalidate for this root term
      $root_term->invalidateCache();
      // invalidate for root term of the previous menu term
      if ($original = $this->original) {
        $new_rt_id = $root_term->id();
        if ($root_term = $original->getRootTerm() && $root_term->id() !== $new_rt_id) {
          $root_term->invalidateCache();
        }
      }
    }
  }

  /**
   * 
   */
  public function getSubtree() {
    if (!isset($this->menu_subtree)) {
      $parentTid = $this->id();
      $this->menu_subtree = $this->getVocabulary()->getVocabularyMenuTree($this->id());
    }
    return $this->menu_subtree;
  }

  /**
   * Move this term and all sub menu terms
   * 
   * - Move them to position by a target root term
   * - Move them to the top level and first position
   * 
   * @param Drupal\slogtx\Entity\RootTerm $root_term
   */
  public function moveToRootTerm(RootTerm $root_term) {
    if (!$this->isMenuTerm()) {
      $message = t('Only menu terms can be moved: (menu id = %menuid).', ['%menuid' => $this->id()]);
      throw new \LogicException($message);
    }

    $this->invalidateRootTermCache();
    unset($this->vocabulary);
    unset($this->target_term);
    unset($this->root_term);

    $target_rootterm_id = $root_term->id();
    $target_vocabulary = $root_term->getVocabulary();
    $target_vid = $target_vocabulary->id();
    $new_weight = 0;
    $r_tree = $target_vocabulary->getVocabularyMenuTree($target_rootterm_id, 1, FALSE);
    if (!empty($r_tree)) {
      $new_weight = (integer) ($r_tree[0]->weight) - 1;
    }

    $source_vocabulary = $this->getVocabulary();
    $tree = $source_vocabulary->getVocabularyMenuTree($this->id(), 2, TRUE);

    $this->setParentID($target_rootterm_id)
            ->setVocabularyId($target_vid)
            ->setWeight($new_weight)
            ->save();

    if (!empty($tree)) {
      foreach ($tree as $menuterm) {
        $menuterm->setVocabularyId($target_vid)->save();
      }
    }

    // trigger event after moving
    $data = [
        'moved_menu_tid' => $this->id(),
        'source_vid' => $source_vocabulary->id(),
        'source_tbid' => $source_vocabulary->getToolbarId(),
        'target_root_tid' => $root_term->id(),
        'target_vid' => $target_vid,
        'target_tbid' => $target_vocabulary->getToolbarId(),
    ];
    $event = new SlogtxGenericEvent($data);
    \Drupal::service('event_dispatcher')
            ->dispatch(SlogtxEvents::TX_MENU_TERM_MOVED, $event);
  }

}
