<?php

/**
 * @file
 * Contains \Drupal\slogtx\Entity\TxToolbar.
 */

namespace Drupal\slogtx\Entity;

use Drupal\slogtx\SlogTx;
use Drupal\slogtx\Event\SlogtxEvents;
use Drupal\slogtx\Event\TxEntitiesEvent;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\slogtx\Interfaces\TxToolbarInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\slogtx\TxEntityStorageTrait;
use Drupal\slogtx\TxEntityLockTrait;

/**
 * Defines the Node type configuration entity.
 * 
 * For more definitions see slogtx_ui_entity_type_build().
 *
 * @ConfigEntityType(
 *   id = "slogtx_tb",
 *   label = @Translation("Slog toolbar"),
 *   handlers = {
 *     "access" = "Drupal\slogtx\Access\SlogToolbarAccessControlHandler",
 *   },
 *   admin_permission = "administer slog taxonomy",
 *   config_prefix = "toolbar",
 *   bundle_of = "slogtx_voc",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "status" = "status",
 *     "weight" = "weight"
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "description",
 *     "enforce_target_entity",
 *     "base_tab_name",
 *     "is_vertical",
 *     "provider",
 *     "status",
 *     "weight",
 *     "locked",
 *   },
 *   additional = {
 *     "slog_is_lockable" = { "delete", "update", "disable" },
 *   },
 * )
 */
class TxToolbar extends ConfigEntityBundleBase implements TxToolbarInterface {

  use TxEntityStorageTrait;

  use TxEntityLockTrait;

  /**
   * The machine name of this slog toolbar.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the node type.
   *
   * @var string
   *
   * @todo Rename to $label.
   */
  protected $name;

  /**
   * A brief description of this node type.
   *
   * @var string
   */
  protected $description;

  /**
   * Target Entity type to be enforced if enforced.
   *
   * @var mixed 
   *  Target entity type as string if enforced, FALSE otherwise
   */
  protected $enforce_target_entity = FALSE;

  /**
   * Base tab name for auto created vocabularies.
   *
   * @var mixed optional
   *  The first character of toolbar name if not set
   */
  protected $base_tab_name = NULL;

  /**
   * Whether the toolbar is vertical or horizontal.
   *
   * @var boolean
   *  TRUE for vertical toolbar, FALSE for horizontal (default)
   */
  protected $is_vertical = FALSE;

  /**
   * The weight of this vocabulary in relation to other vocabularies.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * The module which the toolbar defines.
   *
   * @var string
   */
  protected $provider = 'slogtx';
  protected $locked = FALSE;
  protected $onDelete = FALSE;
  protected $cacheRootTermIDs = NULL;
  protected $allowedVocabIds = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);
    if (!defined('MAINTENANCE_MODE')) {
      SlogTx::setEntityInstance($entity_type, $this->id(), $this);
    }
  }

  /**
   * Return the slogtx vocabulary storage object.
   * 
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public function storageVocab() {
    return SlogTx::entityStorage('slogtx_voc');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnforceTargetEntity() {
    return $this->enforce_target_entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getTbTabBase() {
    $btname = !empty($this->base_tab_name) ? $this->base_tab_name : $this->id()[0];
    return strtolower($btname);
  }

  public function isMenuToolbar() {
    return (boolean) $this->enforce_target_entity;
  }

  public function isGlobalMenuToolbar() {
    return ($this->enforce_target_entity === SlogTx::TARGETENTITY_ID_NONE);
  }

  public function getSysMenuTbRootTermPluginId($base_id, $target_tb_id = NULL) {
    $args = [
        '@id' => $this->id(),
        '@base_id' => $base_id,
        '@ttb_id' => ($target_tb_id ?: 'NULL'),
    ];
    if ($this->isSysToolbar()) {
      if ($target_tb_id && $target_tb = SlogTx::getToolbar($target_tb_id)) {
        $vid = $target_tb->getSysToolbarVid($base_id);
        return "$base_id:$vid";
      }
    }
    elseif ($this->isMenuToolbar()) {
      $vid = $this->getSysToolbarVid($base_id);
      return "$base_id:$vid";
    }

    throw new \LogicException(t('Root term plugin id not get: @id.@base_id.@ttb_id', $args));
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    return $this->set('weight', $weight);
  }

  public function isEnabled() {
    return (boolean) $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function isVertical() {
    return $this->is_vertical;
  }

  public function getDirectionLabel() {
    return $this->isVertical() ? t('vertical') : t('horizontal');
  }

  /**
   * {@inheritdoc}
   */
  public function isSysToolbar() {
    return ($this->id() === SlogTx::TOOLBAR_ID_SYS);
  }

  /**
   * {@inheritdoc}
   */
  public function isNodeSubsysToolbar() {
    return ($this->id() === SlogTx::TOOLBAR_ID_NODE_SUBSYS);
  }

  /**
   * {@inheritdoc}
   */
  public function isUnderscoreToolbar() {
    $toolbar = $this->id();
    return ($toolbar[0] === SlogTx::TOOLBAR_UNDERSCORE);
  }

  /**
   * {@inheritdoc}
   */
  public function pathLabel() {
    return $this->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return $this->provider;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    $this->triggerToolbarChanged();
    SlogTx::resetToolbarsSorted();
    if (!$update && $this->locked) {
      $this->lock($this->locked, $this->provider);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function triggerToolbarChanged() {
    $toolbar_id = $this->id();
    $event = new TxEntitiesEvent([$toolbar_id => $this]);
    \Drupal::service('event_dispatcher')->dispatch(SlogtxEvents::TX_TOOLBAR_CHANGED, $event);
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);

    // delete all vocabularies which belongs to the toolbar
    foreach ($entities as $toolbar) {
      // delete locks
      $toolbar->lock(self::getAllAllowedOperations(), NULL);
      $toolbar->triggerToolbarChanged();
      $this->onDelete = TRUE;
      foreach ($toolbar->getVocabularies(TRUE, FALSE) as $vocabulary) {
        $vocabulary->delete();
      }
      $this->onDelete = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabularyIds($all = FALSE, $status = TRUE) {
    $query = $this->storageVocab()
            ->getQuery()
            ->condition('bundle', $this->id())
            ->sort('weight', 'ASC');
    if (!$all) {
      $query->condition('status', $status);
    }

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabularies($all = FALSE, $status = TRUE) {
    $vocab_ids = $this->getVocabularyIds($all, $status);
    $vocabs = $this->storageVocab()->loadMultiple($vocab_ids);
    if ($this->onDelete) {
      return $vocabs;
    }

    $result = [];
    foreach ($vocabs as $vid => $vocabulary) {
      if ($this->isValidVocabID($vid) && $vocabulary->isValid()) {
        $result[$vid] = $vocabulary;
      }
    }

    return $result;
  }

  public function isValidVocabID($vid) {
    if ($this->isMenuToolbar() && !$this->isUnderscoreToolbar()) {
      list($toolbar_id, $toolbartab) = SlogTx::getVocabularyIdParts($vid);
      $toolbartab = strtolower($toolbartab);
      if (empty($this->allowedVocabIds)) {
        $tabbase = $this->getTbTabBase();
        $max_toolbar_tabs = SlogTx::getMaxToolbarTabs();
        $this->allowedVocabIds = [];
        for ($idx = 0; $idx < $max_toolbar_tabs; $idx++) {
          $tbtab_idx = $idx + 1;
          $this->allowedVocabIds[] = $tabbase . $tbtab_idx;
        }
      }
      
      // valid if allowed
      return in_array($toolbartab, $this->allowedVocabIds);
    }

    return TRUE;
  }

  /**
   * Whether the tollbar has vocabularies with (current) menu items.
   * 
   * @return boolean
   */
  public function hasVisibleVocabularies() {
    foreach ($this->getVocabularies(TRUE) as $vocabulary) {
      if ($vocabulary->isEnabled() && $vocabulary->currentHasMenuitems()) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Whether one of the tabs has the target entity id.
   * 
   * @param string $target_entity_id
   * @param boolean $all
   * @param boolean $status
   * @return boolean
   */
  public function hasTargetEntityTab($target_entity_id, $all = FALSE, $status = TRUE) {
    foreach ($this->getVocabularies($all, $status) as $vocabulary) {
      if ($vocabulary->getTargetEnityTypeId() === $target_entity_id) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Whether one of the active tabs has the target entity id 'user_role'.
   * 
   * @return boolean
   */
  public function hasRoleTargetEntity() {
    return $this->hasTargetEntityTab('user_role');
  }

  /**
   * Return all current root term ids of active vocabularies with menu items .
   * 
   * @param boolean $as_string
   *  Return result as string
   * @param boolean $reset
   *  Do reset cached result
   * @return array|string
   */
  public function getCurrentRootTermIDs($as_string = FALSE, $reset = FALSE) {
    if ($reset) {
      unset($this->cacheRootTermIDs);
    }
    if (!isset($this->cacheRootTermIDs)) {
      $ids = [];
      foreach ($this->getVocabularies() as $vocabulary) {
        if ($vocabulary->currentHasMenuitems(TRUE, $reset)) {
          $current_rt = $vocabulary->getCurrentRootTerm();
          $ids[] = $current_rt->id();
        }
      }
      $this->cacheRootTermIDs = $ids;
    }

    if ($as_string) {
      return implode('-', $this->cacheRootTermIDs);
    }
    return $this->cacheRootTermIDs;
  }

  public function getSysToolbarVid($base_toolbartab, &$toolbartab = NULL) {
    $toolbar_id = $this->id();
    $tb_part = $this->isUnderscoreToolbar() ? 'sys' . $toolbar_id : $toolbar_id;
    $toolbartab = "{$base_toolbartab}_{$tb_part}";
    $vid = SlogTx::getVocabularyIdFromParts([SlogTx::TOOLBAR_ID_SYS, $toolbartab]);
    return $vid;
  }

}
