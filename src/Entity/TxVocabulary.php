<?php

/**
 * @file
 * Contains \Drupal\slogtx\Entity\TxVocabulary.
 */

namespace Drupal\slogtx\Entity;

use Drupal\slogtx\SlogTx;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\slogtx\Entity\RootTerm;
use Drupal\slogtx\Interfaces\TxVocabularyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\slogtx\TxEntityStorageTrait;
use Drupal\slogtx\TxEntityLockTrait;

/**
 * Defines the slog vocabulary entity.
 *
 * For more definitions see slogtx_ui_entity_type_build().
 *
 * @ConfigEntityType(
 *   id = "slogtx_voc",
 *   label = @Translation("Slog vocabulary"),
 *   handlers = {
 *     "access" = "Drupal\slogtx\Access\SlogVocabularyAccessControlHandler",
 *     "storage" = "Drupal\taxonomy\VocabularyStorage",
 *   },
 *   admin_permission = "administer slog taxonomy",
 *   config_prefix = "vocabulary",
 *   bundle_entity_type = "slogtx_tb",
 *   bundle_of = "slogtx_tt",
 *   entity_keys = {
 *     "id" = "vid",
 *     "label" = "name",
 *     "bundle" = "bundle",
 *     "status" = "status",
 *     "weight" = "weight"
 *   },
 *   config_export = {
 *     "vid",
 *     "name",
 *     "description",
 *     "provider",
 *     "bundle",
 *     "target_entity_plugin_id",
 *     "icon_id",
 *     "weight",
 *     "status",
 *     "locked",
 *   },
 *   additional = {
 *     "slog_is_lockable" = { "delete", "update", "disable" },
 *   },
 * )
 * 
 */
class TxVocabulary extends Vocabulary implements TxVocabularyInterface {

  use StringTranslationTrait;

  use TxEntityStorageTrait;

  use TxEntityLockTrait;

  /**
   * Avoid circular handling and not allowed handling when a target term is just added
   * 
   * @var array For certain vocabulary the value is TRUE if on adding target term
   */
  protected static $on_adding_target_term = [];

  /**
   * The slog vocabulary bundle (e.g. tbtop in tbtop__favorit).
   *
   * @var string
   */
  public $bundle = '';

  /**
   * The weight for sorting within the toolbar.
   *
   * @var integer
   */
  protected $weight = 0;

  /**
   * The slog vocabulary's target entity type id (e.g. node, user, user_role).
   *
   * @var string
   */
  public $target_entity_plugin_id = SlogTx::TARGETENTITY_ID_NONE;

  /**
   * The slog vocabulary's icon id.
   * 
   * For customising the vocabulary's icon: class = 'toolbar-icon-' . $icon_id.
   * 
   * @var string
   */
  public $icon_id = 'menu';

  /**
   * Cache for the target entity type object.
   *
   * @var string
   */
  public $target_entity_plugin;

  /**
   * The module which the vocabulary defines.
   *
   * @var string
   */
  protected $provider = 'slogtx';
  protected $target_tid;  // cached target term id
  protected $target_term; // cached target term object

  /**
   * The name of the current root term.
   * 
   * - The current root term is a term within the current target term
   * - Root term names may be 'Default', 'Trash', 'Temp', ...
   *
   * The current root term name is stored as state in key_value table for each target term.
   * 
   * @var string
   */
  protected $currentRootTermName;

  /**
   * Cache for got information.
   * 
   * Is populated by self::isValidTargetTermName()
   * 
   * @var array 
   *  Each item with target term name as key and term object as value
   */
  protected $valid_target_terms = [];

  /**
   * Cache for got information.
   * 
   * Is populated by self::isValidRootTermName()
   * 
   * @var array 
   *  Each item with root term name as key and term object as value
   */
  protected $valid_root_terms = NULL;

  /**
   * Cached toolbar object to which the vocabulary belongs to.
   *
   * @var \Drupal\slogtx\Entity\TxToolbar 
   */
  protected $toolbar = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);
    if (!defined('MAINTENANCE_MODE')) {
      SlogTx::setEntityInstance($entity_type, $this->id(), $this);
    }
  }

  /**
   * Return the slogtx target term storage object.
   * 
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public function storageTT() {
    return SlogTx::entityStorage('slogtx_tt');
  }

  /**
   * Return the slogtx root term storage object.
   * 
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public function storageRT() {
    return SlogTx::entityStorage('slogtx_rt');
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    return $this->set('weight', $weight);
  }

  /**
   * {@inheritdoc}
   * //todo::interface.entry
   */
  public function getTargetEnityTypeId() {
    return $this->target_entity_plugin_id;
  }

  public function setTargetEnityTypeId($target_entity_plugin_id) {
    if (!SlogTx::hasTargetEntityId($target_entity_plugin_id)) {
      $message = t('Unvalid or disabled target entity type: %type.', ['%type' => $target_entity_plugin_id]);
      throw new \LogicException($message);
    }

    $this->target_entity_plugin_id = $target_entity_plugin_id;
    unset($this->target_entity_plugin);
  }

  /**
   * {@inheritdoc}
   * //todo::interface.entry
   */
  public function getTargetEntityPlugin() {
    if (!isset($this->target_entity_plugin)) {
      // set target_entity_plugin_id with exception
      $this->setTargetEnityTypeId($this->target_entity_plugin_id);
      // !!! notice: do not use SlogTx::getTargetEntityPlugin() - is cached and without vocabulary
      $this->target_entity_plugin = SlogTx::pluginManager('target_entity')
              ->createInstance($this->target_entity_plugin_id, ['vocabulary' => $this]);
      if (empty($this->target_entity_plugin)) {
        $this->target_entity_plugin = SlogTx::pluginManager('target_entity')
                ->createInstance(SlogTx::TARGETENTITY_ID_INVALID, ['vocabulary' => $this]);
      }
      if (empty($this->target_entity_plugin)) {
        $this->target_entity_plugin = FALSE;
      }
    }

    return $this->target_entity_plugin;
  }

  /**
   * {@inheritdoc}
   * //todo::interface.entry
   */
  public function getTargetEntityName() {
    return $this->getTargetEntityPlugin()->getTitle();
  }

  /**
   * Whether this vocabulary is valid, has a valid target entity plugin.
   */
  public function isValid() {
    return $this->getTargetEntityPlugin()->isValid();
  }

  /**
   * Whether the status is 1
   */
  public function isEnabled() {
    return !empty($this->status);
  }

  /**
   * {@inheritdoc}
   * //todo::interface.entry
   */
  public function isOfTargetEntityType($type) {
    return ($this->target_entity_plugin_id === $type);
  }

  public function isNoneTargetEntity() {
    return $this->isOfTargetEntityType(SlogTx::TARGETENTITY_ID_NONE);
  }

  /**
   * {@inheritdoc}
   * //todo::interface.entry
   */
  public function isNodeTargetEntity() {
    return $this->isOfTargetEntityType('node');
  }

  /**
   * {@inheritdoc}
   * //todo::interface.entry
   */
  public function isUserTargetEntity() {
    return $this->isOfTargetEntityType('user');
  }

  /**
   * {@inheritdoc}
   * //todo::interface.entry
   */
  public function isRoleTargetEntity() {
    return $this->isOfTargetEntityType('user_role');
  }

  /**
   * {@inheritdoc}
   */
  public function isSysToolbar() {
    return $this->getToolbar()->isSysToolbar();
  }

  /**
   * {@inheritdoc}
   */
  public function isNodeSubsysToolbar() {
    return $this->getToolbar()->isNodeSubsysToolbar();
  }

  /**
   * {@inheritdoc}
   */
  public function isUnderscoreToolbar() {
    return $this->getToolbar()->isUnderscoreToolbar();
  }

  /**
   * Return the toolbar tab label, overridden or own.
   * 
   * @return string
   */
  public function getTbtabLabel($add_entity_label = FALSE) {
    return $this->getTargetEntityPlugin()->getTbtabLabel($add_entity_label);
  }

  /**
   * Return the toolbar tab description, overridden or own.
   * 
   * @return string
   */
  public function getTbtabDescription() {
    $default = $this->getDescription();
    return $this->getTargetEntityPlugin()->getTbtabDescription($default);
  }

  /**
   * Set the plugin current entity, explicit or from any current.
   * 
   * @param EntityInterface $entity
   */
  public function setCurrentTargetEntity($entity = NULL) {
    if ($entity) {
      $this->getTargetEntityPlugin()->setEntity($entity);
    }
    else {
      $this->getTargetEntityPlugin()->setDefaultEntity();
    }
  }

  /**
   * Return path label of this vocabulary, 
   * i.e. toolbarLabel/vocabularyLabel.
   * 
   * @return string
   */
  public function pathLabel() {
    if ($toolbar = $this->getToolbar()) {
      $path = [$toolbar->label(), $this->label()];
      return implode('/', $path);
    }
    return $this->id();
  }

  /**
   * {@inheritdoc}
   */
  public function headerPathLabel() {
    return $this->getTbtabLabel(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getToolbarId() {
    return $this->bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getToolbarName() {
    return $this->getToolbar()->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getToolbar() {
    if (!$this->toolbar) {
      $this->toolbar = SlogTx::entityInstance('slogtx_tb', $this->getToolbarId());
    }
    return $this->toolbar;
  }

  public function getTargetTermIdByName(string $name, $reset = FALSE, $do_create = FALSE) {
    if (!empty($name)) {
      $storage = $this->storageTT();
      $vid = $this->id();
      if (!$storage->hasTargetTermByName($vid, $name, $reset)) {
        if ($do_create && !$storage->hasTargetTermByName($vid, $name, TRUE)) {
          $entity = $this->getEntityFromTargetTermName($name);
          if ($entity && $entity instanceof EntityInterface) {
            $args = [
                '@entity_label' => $entity->label(),
                '@vid' => $this->id(),
            ];
            $values = [
                'vid' => $this->id(),
                'name' => $name,
                'description' => t('@entity_label: TbTab for @vid', $args),
                'parent' => 0,
                'langcode' => $this->langcode,
                'status' => TRUE,
                'locked' => TRUE,
            ];
            $this->addTargetTerm($values);
            $reset = TRUE;
          }
        }
        else {
          // no fallback
          return FALSE;
        }
      }

      $target_term_id = $storage->getTargetTermIdByName($vid, $name, $reset);
      if (!$target_term_id && $name === SlogTx::TARGETTERM_DEFAULT) {
        $this->addDefaultTargetTerm();
        $target_term_id = $storage->getTargetTermIdByName($vid, $name, TRUE);
      }
    }

    if (empty($target_term_id)) {
      $args = [
          '@voc' => $this->id(),
          '@name' => $name,
      ];
      $message = t("Target term not found: @voc/@name", $args);
      throw new \LogicException($message);
    }

    return $target_term_id ?? FALSE;
  }

  public function getTargetTermByName($tt_name, $reset = FALSE, $do_create = FALSE) {
    if ($do_create) {
      $reset = TRUE;
    }
    if ($reset) {
      unset($this->valid_target_terms[$tt_name]);
    }
    if (!isset($this->valid_target_terms[$tt_name])) {
      $target_tid = $this->getTargetTermIdByName($tt_name, $reset, $do_create);
      $this->valid_target_terms[$tt_name] = ($target_tid ? $this->storageTT()->load($target_tid) : NULL);
    }

    return $this->valid_target_terms[$tt_name];
  }

  /**
   * //todo::text - hasTargetTermByName() and more
   * 
   * @param string $name
   * @return boolean
   */
  public function hasTargetTermByName($name, $reset = FALSE) {
    return $this->storageTT()->hasTargetTermByName($this->id(), $name, $reset);
  }

  /**
   * {@inheritdoc}
   */
  public function getRootTermIds($target_tid = NULL, $all = FALSE, $status = TRUE) {
    if (!isset($target_tid)) {
      $target_tid = $this->getCurrentTargetTermTid();
    }
    $ids = $this->storageRT()->getRootTermIds($this->id(), $target_tid);
    if (!$all) {
      $terms = $this->storageRT()->loadMultiple($ids);
      $ids = [];
      foreach ($terms as $tid => $term) {
        if ($term->getStatus() == $status) {
          $ids[$tid] = $tid;
        }
      }
    }

    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function getRootTerms($target_tid = NULL, $all = FALSE, $status = TRUE) {
    $root_tids = $this->getRootTermIds($target_tid, $all, $status);
    return $this->storageRT()->loadMultiple($root_tids);
  }

  public function getRootTermIdByName($name, $target_tid = NULL, $reset = FALSE) {
    if (empty($target_tid)) {
      $target_tid = $this->getCurrentTargetTermTid();
    }
    return $this->storageRT()->getRootTermIdByName($this->id(), $target_tid, $name, $reset);
  }

  public function getRootTermByName($name, $target_tid = NULL, $reset = FALSE) {
    if (!empty($name)) {
      $root_tid = $this->getRootTermIdByName($name, $target_tid, $reset);
      return ($root_tid ? $this->storageRT()->load($root_tid) : NULL);
    }

    return NULL;
  }

  /**
   * Return an array of terms (definitions or entities) or FALSE for no terms
   * 
   * //todo::text
   * // for current target term only
   * 
   * @param string|UserInterface|RoleInterface $target
   * @param string $rootterm_name (e.g. 'Default', 'Temp', ...)
   * @param integer $max_depth
   * @param boolean $load_entities
   * @return array|FALSE
   *  array of stdClass or \Drupal\taxonomy\Entity\Term
   */
  public function getRootTermTree($rootterm_name, $max_depth = NULL, $load_entities = FALSE) {
    // valid root term only
    if ($this->isValidRootTermName($rootterm_name)) {
      // get the root term id within the current target term
      $rootterm_tid = $this->getRootTermIdByName($rootterm_name);
      if ($rootterm_tid > 0) {
        // return vocabulary tree for target and root term
        return $this->getVocabularyMenuTree($rootterm_tid, $max_depth, $load_entities);
      }
    }

    // tree not found
    return [];
  }

  /**
   * Load a target term directly from storage,
   * 
   * @param integer $target_id
   * @return \Drupal\slogtx\Entity\TargetTerm
   */
  public function getTargetTerm($target_id) {
    return $this->storageTT()->load($target_id);
  }

  /**
   * Wrapper function for SlogTx::getVocabularyMenuTree
   */
  public function getVocabularyMenuTree($parent, $max_depth = NULL, $load_entities = FALSE) {
    return SlogTx::getVocabularyMenuTree($this->id(), $parent, $max_depth, $load_entities);
  }

  /**
   * Set current target term and current root term by root term id.
   * 
   * @param mixed $slogtx_rt
   *  Root term object or root term id
   * @return boolean
   *  TRUE on success, FALSE otherwise
   */
  public function setCurrentByRootTerm($slogtx_rt) {
    $root_term = SlogTx::entityInstance('slogtx_rt', $slogtx_rt);
    if ($root_term instanceof RootTerm && $this->id() == $root_term->getVocabularyId()) {
      $target_term = $root_term->getTargetTerm();
      $this->setCurrentTargetTermName($target_term->label());
      $this->setCurrentRootTermName($root_term->label());
      return TRUE;
    }

    SlogTx::logger()->error(t('Current by root term not set.'));
    return FALSE;
  }

  public function setCurrentTargetTermName(string $term_name) {
    $cur_tt_name = $this->getCurrentTargetTermName();
    if ($term_name !== $cur_tt_name) {
      $this->resetRootTermCache();
      return $this->getTargetEntityPlugin()->setCurrentTargetTermName($term_name);
    }
    // no change
    return $cur_tt_name;
  }

  public function getCurrentTargetTermName() {
    return $this->getTargetEntityPlugin()->getCurrentTargetTermName();
  }

  public function getEntityFromTargetTermName(string $term_name) {
    return $this->getTargetEntityPlugin()->getEntityFromTargetTermName($term_name);
  }

  /**
   * Return the term id of the current target term.
   * 
   * @return integer
   *  Returns -1 if there is no current target term
   */
  public function getCurrentTargetTermTid() {
    $term_name = $this->getCurrentTargetTermName();
    return $this->getTargetTermIdByName($term_name);
  }

  /**
   * Return an instance of the current target term.
   * 
   * @param boolean $reset 
   *  Reset storage cache
   * @return \Drupal\slogtx\Entity\TargetTerm
   */
  public function getCurrentTargetTerm($reset = FALSE) {
    $term_name = $this->getCurrentTargetTermName();
    return $this->getTargetTermByName($term_name, $reset);
  }

  /**
   * Whether the term name is a valid target term name. 
   * 
   * - Only enabled target terms are valid
   * 
   * @param string $term_name
   * @return boolean
   *  TRUE if valid, FALSE otherwise.
   */
  public function isValidTargetTermName($term_name) {
    if (empty($term_name)) {
      return FALSE;
    }

    $target_term = $this->getTargetTermByName($term_name);
    return !empty($target_term);
  }

  /**
   * Return the term name of the current root term.
   * 
   * The term exists and is enabled (is validated)
   * 
   * @return string|FALSE
   *  Returns FALSE if there is no current root term
   */
  public function getCurrentRootTermName() {
    if (!isset($this->currentRootTermName)) {
      $this->currentRootTermName = $this->setCurrentRootTermName(SlogTx::ROOTTERM_DEFAULT);
    }

    return $this->currentRootTermName;
  }

  /**
   * Set a new current root term name.
   * 
   * ????
   *  
   * @param string $term_name
   *  The new root term name to set
   * @return string|FALSE
   *  The (new) current root term name or FALSE if there is none
   */
  public function setCurrentRootTermName($term_name) {
    if (empty($term_name)) {
      $term_name = SlogTx::ROOTTERM_DEFAULT;
    }
    
    if ($term_name !== $this->currentRootTermName) {
      $this->currentRootTermName = FALSE;
      if ($this->isValidRootTermName($term_name)) {
        $this->currentRootTermName = $term_name;
      }
    }

    return $this->currentRootTermName;
  }

  public function resetRootTermCache() {
    $this->currentRootTermName = NULL;
    $this->valid_root_terms = NULL;
    $this->storageTT()->resetCacheByVocabulary($this->id());
  }

  /**
   * Return an instance of the current root term.
   * 
   * @param boolean $reset 
   *  Reset storage cache
   * @return \Drupal\slogtx\Entity\RootTerm or NULL
   */
  public function getCurrentRootTerm($reset = FALSE) {
    if ($this->isOnAddingTargetTerm()) {
      return NULL;  // avoid not allowed handling
    }
    $term_name = $this->getCurrentRootTermName();
    return $this->getRootTermByName($term_name, NULL, $reset);
  }

  /**
   * Whether the current root term has children.
   * 
   * @param boolean $reset
   *  Reset storage cache
   * @return boolean
   */
  public function currentHasMenuitems($visible_only = TRUE, $reset = FALSE) {
    if ($this->isOnAddingTargetTerm()) {
      return FALSE; // avoid not allowed handling
    }
    $root_term = $this->getCurrentRootTerm($reset);
    if (empty($root_term) || ($visible_only && !$root_term->isVisible())) {
      return FALSE;
    }
    return $root_term->hasChildren($reset);
  }

  /**
   * Whether the term name is a valid root term name. 
   * 
   * ????
   * 
   */
  public function isValidRootTermName($term_name, $target_tid = NULL) {
    if (empty($term_name)) {
      return FALSE;
    }

    if (!isset($target_tid)) {
      $target_tid = $this->getCurrentTargetTermTid();
    }
    if (!isset($this->valid_root_terms)) {
      $this->valid_root_terms = [];
      foreach ($this->getRootTerms($target_tid, TRUE) as $root_term) {
        $name = $root_term->label();
        $this->valid_root_terms[$name] = $root_term;
      }
    }

    return !empty($this->valid_root_terms[$term_name]);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    $enforce_teid = $this->getToolbar()->getEnforceTargetEntity();
    if (!empty($enforce_teid) && $this->target_entity_plugin_id !== $enforce_teid) {
      $args = [
          '@toolbar' => $this->getToolbar()->id(),
          '@enforced' => $enforce_teid,
          '@given' => $this->target_entity_plugin_id,
      ];
      $msg = t('Target entity type missmatch (toolbar/enforced/given): @toolbar/@enforced/@given', $args);
      throw new \Exception($msg);
    }

    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    $this->triggerToolbarChanged();
    if (!$update) {
      // lock if set
      if (!empty($this->locked)) {
        $this->lock($this->locked, $this->provider);
      }

      // add required root terms
      $this->addRequiredTargetTerms();
    }
  }

  /**
   * 
   */
  public function triggerToolbarChanged() {
    $this->getToolbar()->triggerToolbarChanged();
  }

  /**
   * Add required target terms.
   * 
   * Required target terms are defined by vocabulary's target term plugin.
   */
  private function addRequiredTargetTerms() {
    $storage = $this->storageTT();
    $vid = $this->id();
    $required = $this
            ->getTargetEntityPlugin()
            ->getRequiredTargetTerms();

    foreach ($required as $name => $data) {
      if (!$storage->hasTargetTermByName($vid, $name, TRUE)) {
        $values = [
            'vid' => $vid,
            'name' => $name,
            'description' => $data['description'],
            'parent' => 0,
            'langcode' => $this->langcode,
            'status' => true,
            'locked' => $data['locked'],
        ];
        $this->addTargetTerm($values);
      }
    }
  }

  public function addDefaultTargetTerm() {
    $storage = $this->storageTT();
    $vid = $this->id();
    $name = SlogTx::TARGETTERM_DEFAULT;
    if (!$storage->hasTargetTermByName($vid, $name, TRUE)) {
      $required = $this
              ->getTargetEntityPlugin()
              ->getRequiredTargetTerms();
      if (is_array($required) && is_array($required[$name])) {
        $data = $required[$name];
        $values = [
            'vid' => $vid,
            'name' => $name,
            'description' => $data['description'],
            'parent' => 0,
            'langcode' => $this->langcode,
            'status' => true,
            'locked' => $data['locked'],
        ];
        $this->addTargetTerm($values);
      }
    }
  }

  /**
   * Return if there is actual an target term adding for the this vocabulary
   * 
   * @return boolean
   */
  private function isOnAddingTargetTerm() {
    $vid = $this->id();
    return !empty(self::$on_adding_target_term[$vid]);
  }

  public function addTargetTerm($values) {
    if ($this->isOnAddingTargetTerm()) {
      $this->resetRootTermCache();
      return; // avoid circular handling
    }
    $vid = $this->id();
    self::$on_adding_target_term[$vid] = TRUE;
    $target_term = $this->storageTT()->create($values);
    $target_term->save();
    self::$on_adding_target_term[$vid] = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);
    foreach ($entities as $vocabulary) {
      // delete all vocabulary's locks
      $vocabulary->deleteLockedAll();
      //
      $vocabulary->triggerToolbarChanged();
    }
  }

  /**
   * Delete all lock records for the vocabulary.
   * 
   * There are status and locking data to delete.
   */
  private function deleteLockedAll() {
    // delete vocabulary locks itself (in slogtx.locked.tx)
    // with blocker==NULL the locks will be deleted
    $this->lock(self::getAllAllowedOperations(), NULL);

    // delete locks for all terms within the vocabulary
    $vid = $this->id();
    $keys = SlogTx::dbGetLockedKeys("slogtx.locked.term.{$vid}.%");
    SlogTx::getLockedStorage()->deleteMultiple($keys);
  }

}
