<?php

/**
 * @file
 * Contains \Drupal\slogtx\SlogtxUninstallValidator.
 */

namespace Drupal\slogtx;

use Drupal\slogtx\SlogTx;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Prevents uninstallation of modules providing used toolbars plugins.
 */
class SlogtxUninstallValidator implements ModuleUninstallValidatorInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    $reasons = [];
    if ($module === 'slogtx' && !empty(SlogTx::getToolbars(TRUE))) {
      $items = [];
      $entity_type = \Drupal::entityTypeManager()->getDefinition('slogtx_tb');
      $reasons[] = $this->t('>>>There is content for the entity type: @entity_type. <a href=":url">Remove @entity_type_plural</a>.', [
        '@entity_type' => $entity_type->getLabel(),
        '@entity_type_plural' => $entity_type->getPluralLabel(),
        ':url' => Url::fromRoute('system.prepare_modules_entity_uninstall', ['entity_type_id' => $entity_type->id()])->toString(),
      ]);
    }

    return $reasons;
  }

}
