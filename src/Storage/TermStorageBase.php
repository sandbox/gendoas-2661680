<?php

/**
 * @file
 * Contains \Drupal\slogtx\Storage\TermStorageBase.
 */

namespace Drupal\slogtx\Storage;

use Drupal\taxonomy\TermStorage;
use Drupal\taxonomy\TermStorageInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Defines a base storage controller class for target term, root term and menu term entities.
 */
abstract class TermStorageBase extends TermStorage implements TermStorageInterface {

  /**
   * Whether a term has any children in the hierarchy keyed by term ID.
   * 
   * @var array 
   */
  protected $has_children = [];

  /**
   * Array of all target term ids (all vocabularies).
   * 
   * @var array 
   */
  protected static $all_target_term_ids = NULL;

  /**
   * Array of target term ids keyed by vocabulary ID.
   * 
   * @var array 
   */
  protected $target_term_ids = [];

  /**
   * Target term id keyed by vocabulary ID and target term NAME.
   * 
   * @var array 
   */
  protected $target_term_id_by_name = [];

  /**
   * Array of root term ids keyed by vocabulary ID and target term ID.
   * 
   * @var array 
   */
  protected $root_term_ids = [];

  /**
   * Root term id keyed by vocabulary ID, target term ID and root term NAME.
   * 
   * @var array 
   */
  protected $root_term_id_by_name = [];

  /**
   * Menu term id keyed by vocabulary ID, root term ID and menu term NAME.
   * 
   * @var array 
   */
  protected $menu_term_id_by_name = [];

  /**
   * {@inheritdoc}
   * 
   * Overrides \Drupal\Core\Entity\Sql\SqlContentEntityStorage::onEntityTypeDelete().
   * 
   * Do nothing. Prevent dropping taxonomy tables.
   */
  public function onEntityTypeDelete(EntityTypeInterface $entity_type) {
    // do nothing
    // do not delete this function, 
    // it prevents dropping the taxonomy tables on uninstall.
  }

  /**
   * Return child term ids of the parent.
   * 
   * This depends on term type.
   *  - Target term: parent=0, one level only (i.e. all target terms)
   *  - Root term: parent is a target term, one level only (i.e. all root terms of the target term)
   *  - Menu term: parent is a root term, up to three level (i.e. all menu terms of the root term)
   * 
   * @param string $vid
   *  The vocabulary id of the terms.
   * @param integer $parent 
   *  The parent term id dependent onterm type.
   */
  abstract public function getTreeTermIds($vid, $parent);

  /**
   * {@inheritdoc}
   */
  public function resetCache(array $ids = NULL) {
    //todo::test::resetCache() - parent::resetCache - how expensive ???
    //see \Drupal\Core\Entity\EntityStorageBase::resetCache()
    parent::resetCache($ids);

    // reset own cache too
    // find all vocabularies for the term ids
    // and reset for found vocabularies only
    $vocabs = [];
    foreach ($ids as $tid) {
      $vid = $this->getVocabularyIdFromTermId($tid);
      $vocabs[$vid] = $vid;
    }
    foreach ($vocabs as $vid) {
      $this->resetCacheByVocabulary($vid);
    }
  }

  /**
   * Reset the local cache for a vocabulary's target and root terms.
   * 
   * @param string $vid vocabulary id
   */
  public function resetCacheByVocabulary($vid) {
    unset($this->target_term_ids[$vid]);
    unset($this->target_term_id_by_name[$vid]);
    unset($this->root_term_ids[$vid]);
    unset($this->root_term_id_by_name[$vid]);
  }

  /**
   * Retrieve the id of the first child in hirarchy.
   * 
   * @param integer $tid
   * @return integer or FALSE
   */
  public function getFirstChildTid($tid) {
    $query = $this->database->select('taxonomy_term_field_data', 'fd');
    $this->addExpressionParent($query);
    $result = $query
            ->fields('fd', ['tid'])
            ->condition('p.parent_target_id', $tid)
            ->orderBy('fd.weight')
            ->range(0, 1)
            ->execute()
            ->fetchCol();
    return (!empty($result) && is_array($result) ? $result[0] : FALSE);
  }

  /**
   * Whether the term  has any children at all.
   * 
   * @param integer $tid
   *  Term id
   * @param boolean $reset
   *  Reset local cache
   * @return boolean
   *  TRUE if the term has children, FALSE otherwise.
   */
  public function hasChildren($tid, $reset = FALSE) {
    if ($reset) {
      unset($this->has_children[$tid]);
    }
    if (!isset($this->has_children[$tid])) {
      $this->has_children[$tid] = !empty($this->getFirstChildTid($tid));
    }
    return $this->has_children[$tid];
  }

  /**
   * Whether the vocabulary has a target term with the given name.
   * 
   * @param string $vid
   *  Id of the vocabulary
   * @param string $tt_name
   *  Name of the target term
   * @param boolean $reset
   *  Reset local cache
   * @return boolean
   *  TRUE if the target term exists, FALSE otherwise.
   */
  public function hasTargetTermByName($vid, string $tt_name, $reset = FALSE) {
    $target_term_id = $this->getTargetTermIdByName($vid, $tt_name, $reset);
    return !empty($target_term_id);
  }

  /**
   * Return the target term id of the vocabulary's target term with the given name.
   * 
   * @param string $vid
   *  Id of the vocabulary
   * @param string $tt_name
   *  Name of the target term
   * @param boolean $reset
   *  Reset local cache
   * @return int
   *  The target term id if term exists, NULL otherwise
   */
  public function getTargetTermIdByName($vid, string $tt_name, $reset = FALSE) {
    if ($reset) {
      unset($this->target_term_id_by_name[$vid][$tt_name]);
    }

    if (!isset($this->target_term_id_by_name[$vid][$tt_name])) {
      $this->target_term_id_by_name[$vid][$tt_name] = NULL;
      $result = $this->_getTermByName($vid, 0, $tt_name);

      if (!empty($result)) {
        $this->target_term_id_by_name[$vid][$tt_name] = (integer) $result[0];
      }
    }

    return $this->target_term_id_by_name[$vid][$tt_name];
  }

  /**
   * Return all root term ids of a vocabulary and target term.
   * 
   * - A root term has a target term as the only parent.
   * - The children of the root term are building a hierarchical menu.
   * 
   * @param string $vid 
   *  The ID of a slog vocabulary.
   * @param integer $target_tid
   *   Target term id for which to get the root term ids.
   * @param boolean $reset
   *  Reset local cache
   * @return array of root term ids
   *   An associative array with key equals values.
   */
  public function getRootTermIds($vid, $target_tid, $reset = FALSE) {
    if ($reset) {
      unset($this->root_term_ids[$vid][$target_tid]);
    }
    if (!isset($this->root_term_ids[$vid][$target_tid])) {
      $this->root_term_ids[$vid][$target_tid] = $this->getLevelTermIds($vid, [$target_tid]);
    }

    return $this->root_term_ids[$vid][$target_tid];
  }

  /**
   * Retrieve the vocabulary id of a term.
   * 
   * @param integer $tid
   *  The term id for which to retrieve the parent term id.
   * @return string|boolean
   *  The vocabulary id if found, FALSE otherwise.
   */
  public function getVocabularyIdFromTermId($tid) {
    $query = $this->database->select('taxonomy_term_data', 'td');
    $id = $query
            ->fields('td', ['vid'])
            ->condition('td.tid', $tid)
            ->execute()
            ->fetchCol();
    if (!empty($id)) {
      return $id[0];
    }

    return FALSE;
  }

  /**
   * Retrieve all child term ids of a set of parent ids, one level only.
   * 
   * @param string $vid
   *  Id of the vocabulary
   * @param array $parents
   *  A set of term ids within the vocabulary for which to retrieve children.
   * @return array of child term ids
   *   An associative array with key equals values.
   */
  public function getLevelTermIds($vid, array $parents, $limit = 100) {
    if (empty($parents)) {
      return [];
    }
    
    $parent_table = $this->getParentTable();
    $query = $this->database->select($parent_table, 'p');
    $ids = $query
            ->fields('p', ['entity_id'])
            ->condition('p.bundle', $vid)
            ->condition('p.parent_target_id', $parents, 'IN')
            ->range(0, $limit)
            ->execute()
            ->fetchCol();
    return array_combine($ids, $ids);
  }

  /**
   * Return term ids found by $vid, $parent_tid and $name.
   * 
   * @param string $vid
   * @param integer $parent_tid
   * @param string $name
   * @return array
   */
  protected function _getTermByName($vid, $parent_tid, $name) {
    $query = $this->database->select('taxonomy_term_field_data', 'fd');
    $this->addExpressionParent($query);
    $result = $query
            ->fields('fd', ['tid'])
            ->condition('fd.vid', $vid)
            ->condition('fd.name', $name)
            ->condition('fd.default_langcode', 1)
            ->condition('p.parent_target_id', $parent_tid)
            ->execute()
            ->fetchCol();

    return $result;
  }

  /**
   * Whether a vocabulary's target term has a root term with the given name.
   * 
   * @param string $vid
   *  Id of the vocabulary
   * @param integer $target_tid
   *   Target term id for which to get the root term object.
   * @param string $name
   *  Name of the root term
   * @param boolean $reset
   *  Reset local cache
   * @return boolean
   *  TRUE if the root term exists, FALSE otherwise.
   */
  public function hasRootTermByName($vid, $target_tid, string $name, $reset = FALSE) {
    if ($reset) {
      unset($this->root_term_id_by_name[$vid][$target_tid][$name]);
    }

    if (!isset($this->root_term_id_by_name[$vid][$target_tid][$name])) {
      $this->root_term_id_by_name[$vid][$target_tid][$name] = FALSE;
      $result = $this->_getTermByName($vid, $target_tid, $name);

      if (!empty($result)) {
        $this->root_term_id_by_name[$vid][$target_tid][$name] = (integer) $result[0];
      }
    }

    return !empty($this->root_term_id_by_name[$vid][$target_tid][$name]);
  }

  /**
   * Return the root term id of the target term's root term of the given name.
   * 
   * @param string $vid
   *  Id of the vocabulary
   * @param integer $target_tid
   *   Target term id for which to validate the root term name.
   * @param string $name
   *  Name of the root term
   * @param boolean $reset
   *  Reset local cache
   * @return int
   *  The root term id if term exists, -1 else
   */
  public function getRootTermIdByName($vid, $target_tid, string $name, $reset = FALSE) {
    if ($this->hasRootTermByName($vid, $target_tid, $name, $reset)) {
      // The id is set by hasRootTermByName
      return $this->root_term_id_by_name[$vid][$target_tid][$name];
    }

    return FALSE;
  }

  /**
   * Whether a vocabulary's root/menu term has a menu term with the given name.
   * 
   * @param string $vid
   *  Id of the vocabulary
   * @param integer $parent_id
   *   Root/menu term id for which to get the menu term object.
   * @param string $name
   *  Name of the menu term
   * @param boolean $reset
   *  Reset local cache
   * @return boolean
   *  TRUE if the menu term exists, FALSE otherwise.
   */
  public function hasMenuTermByName($vid, $parent_id, string $name, $reset = FALSE) {
    if ($reset) {
      unset($this->menu_term_id_by_name[$vid][$parent_id][$name]);
    }

    if (!isset($this->menu_term_id_by_name[$vid][$parent_id][$name])) {
      $this->menu_term_id_by_name[$vid][$parent_id][$name] = FALSE;
      $result = $this->_getTermByName($vid, $parent_id, $name);

      if (!empty($result)) {
        $this->menu_term_id_by_name[$vid][$parent_id][$name] = (integer) $result[0];
      }
    }

    return !empty($this->menu_term_id_by_name[$vid][$parent_id][$name]);
  }

  /**
   * Return the menu term id of the root/menu term's menu term of the given name.
   * 
   * @param string $vid
   *  Id of the vocabulary
   * @param integer $parent_id
   *   Root/menu term id for which to validate the menu term name.
   * @param string $name
   *  Name of the menu term
   * @param boolean $reset
   *  Reset local cache
   * @return int
   *  The menu term id if term exists, -1 else
   */
  public function getMenuTermIdByName($vid, $parent_id, string $name, $reset = FALSE) {
    if ($this->hasMenuTermByName($vid, $parent_id, $name, $reset)) {
      // The id is set by hasMenuTermByName
      return $this->menu_term_id_by_name[$vid][$parent_id][$name];
    }

    return -1;
  }

  public function getParentTable() {
    return $this->getEntityTypeId() . '__parent';
  }

  public function addExpressionParent($query) {
    $parent_table = $this->getParentTable();
    $query->join($parent_table, 'p', 'fd.tid = p.entity_id');
    $query->addExpression('parent_target_id', 'parent');
  }

  /**
   * {@inheritdoc}
   * 
   * Overrides \Drupal\taxonomy\TermStorage::loadTree()
   * 
   * - Prevent loading all vocabulary terms.
   * - Maybe there is a huge amount of terms, so we have to be aware of it.
   */
  public function loadTree($vid, $parent = 0, $max_depth = NULL, $load_entities = FALSE) {
    $cache_key = implode(':', func_get_args());
    if (!isset($this->trees[$cache_key])) {
      // We cache trees, so it's not CPU-intensive to call on a term and its
      // children, too.
//todo::cleanup
//                              if (!isset($this->treeChildren[$vid][$parent])) {
//                                $this->treeChildren[$vid][$parent] = [];
//                                $this->treeParents[$vid] = [];
//                                $this->treeTerms[$vid] = [];
//
//                                // get the storage type dependent term ids
//                                // so we prevent loading all vocabulary terms
//                                $term_ids = $this->getTreeTermIds($vid, $parent);
//                                if (!empty($term_ids)) {
//                                  $query = $this->database->select('taxonomy_term_field_data', 't');
//                                  $query->join('taxonomy_term_hierarchy', 'h', 'h.tid = t.tid');
//                                  $result = $query
//                                      ->addTag('term_access')
//                                      ->fields('t')
//                                      ->fields('h', ['parent'])
//                                      ->condition('t.tid', $term_ids, 'IN')
//                                      ->orderBy('t.weight')
//                                      ->orderBy('t.name')
//                                      ->execute();
//                                  foreach ($result as $term) {
//                                    $this->treeChildren[$vid][$term->parent][] = $term->tid;
//                                    $this->treeParents[$vid][$term->tid][] = $term->parent;
//                                    $this->treeTerms[$vid][$term->tid] = $term;
//                                  }
//                                  foreach ($term_ids as $term_id) {
//                                    if (!isset($this->treeChildren[$vid][$term_id])) {
//                                      $this->treeChildren[$vid][$term_id] = FALSE;
//                                    }
//                                  }
//                                }
//                              }
      if (!isset($this->treeChildren[$vid][$parent])) {
        $this->treeChildren[$vid][$parent] = [];
        $this->treeParents[$vid] = [];
        $this->treeTerms[$vid] = [];

        // get the storage type dependent term ids
        // so we prevent loading all vocabulary terms
        $term_ids = $this->getTreeTermIds($vid, $parent);
        if (!empty($term_ids)) {
          $query = $this->database->select('taxonomy_term_field_data', 'fd');
//          $query->join('taxonomy_term_hierarchy', 'h', 'h.tid = t.tid');
//                          $parent_table = $this->parentTableName();
//                          $query->join($parent_table, 'p', 't.tid = p.entity_id');
//                          $query->addExpression('parent_target_id', 'parent');
          $this->addExpressionParent($query);
//end new
          $result = $query
                  ->addTag('taxonomy_term_access')
                  ->fields('fd')
                  ->condition('fd.tid', $term_ids, 'IN')
//                  ->condition('p.parent_target_id', $parent)
                  ->condition('fd.default_langcode', 1)
                  ->orderBy('fd.weight')
                  ->orderBy('fd.name')
                  ->execute();

//          $xx_cols = $result->fetchCol();

          foreach ($result as $term) {
            $this->treeChildren[$vid][$term->parent][] = $term->tid;
            $this->treeParents[$vid][$term->tid][] = $term->parent;
            $this->treeTerms[$vid][$term->tid] = $term;
          }
          foreach ($term_ids as $term_id) {
            if (!isset($this->treeChildren[$vid][$term_id])) {
              $this->treeChildren[$vid][$term_id] = FALSE;
            }
          }
        }
      }

      // Load full entities, if necessary. The entity controller statically
      // caches the results.
      $term_entities = [];
      if ($load_entities) {
        $term_entities = $this->loadMultiple(array_keys($this->treeTerms[$vid]));
      }

      $max_depth = (!isset($max_depth)) ? count($this->treeChildren[$vid]) : $max_depth;
      $tree = [];

      // Keeps track of the parents we have to process, the last entry is used
      // for the next processing step.
      $process_parents = [];
      $process_parents[] = $parent;

      // Loops over the parent terms and adds its children to the tree array.
      // Uses a loop instead of a recursion, because it's more efficient.
      while (count($process_parents)) {
        $parent = array_pop($process_parents);
        // The number of parents determines the current depth.
        $depth = count($process_parents);
        if ($max_depth > $depth && !empty($this->treeChildren[$vid][$parent])) {
          $has_children = FALSE;
          $child = current($this->treeChildren[$vid][$parent]);
          do {
            if (empty($child)) {
              break;
            }
            $term = $load_entities ? $term_entities[$child] : $this->treeTerms[$vid][$child];
            if (!$term) {
              $xx = 0;
            }
            if (isset($this->treeParents[$vid][$load_entities ? $term->id() : $term->tid])) {
              // Clone the term so that the depth attribute remains correct
              // in the event of multiple parents.
              $term = clone $term;
            }
            $term->depth = $depth;
            unset($term->parent);
            $tid = $load_entities ? $term->id() : $term->tid;
            $term->parents = $this->treeParents[$vid][$tid];
            $tree[] = $term;
            if (!empty($this->treeChildren[$vid][$tid])) {
              $has_children = TRUE;

              // We have to continue with this parent later.
              $process_parents[] = $parent;
              // Use the current term as parent for the next iteration.
              $process_parents[] = $tid;

              // Reset pointers for child lists because we step in there more
              // often with multi parents.
              reset($this->treeChildren[$vid][$tid]);
              // Move pointer so that we get the correct term the next time.
              next($this->treeChildren[$vid][$parent]);
              break;
            }
          } while ($child = next($this->treeChildren[$vid][$parent]));

          if (!$has_children) {
            // We processed all terms in this hierarchy-level, reset pointer
            // so that this function works the next time it gets called.
            reset($this->treeChildren[$vid][$parent]);
          }
        }
      }
      $this->trees[$cache_key] = $tree;
    }
    return $this->trees[$cache_key];
  }

  /**
   * {@inheritdoc}
   * 
   * Overrides \Drupal\Core\Entity\ContentEntityStorageBase::setPersistentCache()
   * 
   */
  protected function setPersistentCache($entities) {
    if (!$this->entityType->isPersistentlyCacheable()) {
      return;
    }

    foreach ($entities as $id => $entity) {
      $cache_tags = [
          $this->entityTypeId . ":$id",
          'entity_field_info',
      ];
      $this->cacheBackend->set($this->buildCacheId($id), $entity, CacheBackendInterface::CACHE_PERMANENT, $cache_tags);
    }
  }

}
