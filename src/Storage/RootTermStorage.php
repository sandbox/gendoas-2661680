<?php

/**
 * @file
 * Contains \Drupal\slogtx\Storage\RootTermStorage.
 */

namespace Drupal\slogtx\Storage;

use Drupal\taxonomy\TermInterface;

/**
 * Defines a storage controller class for slog target term.
 */
class RootTermStorage extends TermStorageBase {

  /**
   * {@inheritdoc}
   * 
   * Implements \Drupal\slogtx\Storage\TermStorageBase::getTreeTermIds()
   */
  public function getTreeTermIds($vid, $parent) {
    return $this->getLevelTermIds($vid, [$parent]);
  }

  /**
   * {@inheritdoc}
   */
  public function getChildren(TermInterface $term) {
    if (!$term->isTargetTerm()) {
      throw new \Exception('Term missmatch: ' . $term->getEntityTypeId());
    }
    
    $query = \Drupal::entityQuery('slogtx_rt')
      ->condition('parent', $term->id());
    return static::loadMultiple($query->execute());
  }

}
