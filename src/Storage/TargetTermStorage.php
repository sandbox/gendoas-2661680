<?php

/**
 * @file
 * Contains \Drupal\slogtx\Storage\TargetTermStorage.
 */

namespace Drupal\slogtx\Storage;

use Drupal\taxonomy\TermInterface;

/**
 * Defines a storage controller class for slog target term.
 */
class TargetTermStorage extends TermStorageBase {

  /**
   * {@inheritdoc}
   * 
   * Implements \Drupal\slogtx\Storage\TermStorageBase::getTreeTermIds()
   */
  public function getTreeTermIds($vid, $parent) {
    if ($parent == 0) {
      return $this->getLevelTermIds($vid, [0]);
    }
  }
  
  /**
   * {@inheritdoc}
   */
  public function getChildren(TermInterface $term) {
    throw new \Exception('TargetTermStorage->getChildren not allowed: ' . $term->getEntityTypeId());
  }

  /**
   * {@inheritdoc}
   * 
   * Overrides \Drupal\Core\Entity\EntityStorageBase::loadMultiple()
   * 
   * In case of $ids==NULL do not load all terms, but only the target terms.
   */
  public function loadMultiple(array $ids = NULL) {
    if (!isset($ids)) {
      throw new \Exception('Load all target terms is not supported.');
    }
    
    if (count($ids) > 10000) {
      $args = ['%number' => count($ids)];
      SlogTx::logger()->warning('Loading a huge amount of target terms: %number.', $args);
    }
    
    
    return parent::loadMultiple($ids);
  }

}
