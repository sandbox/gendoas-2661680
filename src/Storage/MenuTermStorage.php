<?php

/**
 * @file
 * Contains \Drupal\slogtx\Storage\MenuTermStorage.
 */

namespace Drupal\slogtx\Storage;

use Drupal\taxonomy\TermInterface;

/**
 * Defines a storage controller class for menu terms.
 */
class MenuTermStorage extends TermStorageBase {

  /**
   * {@inheritdoc}
   * 
   * Implements \Drupal\slogtx\Storage\TermStorageBase::getTreeTermIds()
   */
  public function getTreeTermIds($vid, $parent) {
    // collect child tids for up to three level
    $ids = [];
    $t0_ids = $this->getLevelTermIds($vid, [$parent]);
    if (!empty($t0_ids)) {
      $ids += $t0_ids;
      $t1_ids = $this->getLevelTermIds($vid, $t0_ids);
      if (!empty($t1_ids)) {
        $ids += $t1_ids;
        $t2_ids = $this->getLevelTermIds($vid, $t1_ids);
        if (!empty($t2_ids)) {
          $ids += $t2_ids;
        }
      }
    }
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function getChildren(TermInterface $term) {
    if (!$term->isRootTerm() && !$term->isMenuTerm()) {
      throw new \Exception('Term missmatch: ' . $term->getEntityTypeId());
    }
    
    $query = \Drupal::entityQuery('slogtx_mt')
      ->condition('parent', $term->id());
    return static::loadMultiple($query->execute());
  }

}
