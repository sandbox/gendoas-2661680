<?php

/**
 * @file
 * Contains \Drupal\slogtx\Annotation\TxRootTermGetBase.
 */

namespace Drupal\slogtx\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the base plugin annotation object for sys toolbars (e.g. _sys, _node)
 *
 * @Annotation
 */
class TxRootTermGetBase extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The description for new created vocabulary.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $vocabulary_description;

  /**
   * The menu icon for new created vocabulary.
   *
   * @var string
   *
   * @ingroup plugin_translatable
   */
  public $icon_id = 'menu';

  /**
   * The default weight.
   *
   * @var int (optional)
   */
  public $weight = 0;
  
}
