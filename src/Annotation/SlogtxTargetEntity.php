<?php

/**
 * @file
 * Contains \Drupal\slogtx\Annotation\SlogtxTargetEntity.
 */

namespace Drupal\slogtx\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an slog target entity annotation object.
 *
 * Plugin Namespace: Plugin\slogtx\TargetEntity
 *
 * For a working example, see \Drupal\slogtx\Plugin\slogtx\TargetEntity\Node
 *
 * @see \Drupal\slogtx\TargetEntityPluginManager
 * @see \Drupal\slogtx\Plugin\slogtx\TargetEntityInterface
 * @see \Drupal\slogtx\Plugin\slogtx\TargetEntityBase
 *
 * @Annotation
 */
class SlogtxTargetEntity extends Plugin {

  /**
   * The plugin ID.
   * 
   * Must be the id of an entity, except 'none' for no entity.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the provider that owns the filter.
   *
   * @var string
   */
  public $provider;

  /**
   * The human-readable name of the filter.
   *
   * This is used as an administrative summary of what the filter does.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * Additional administrative information about the filter's behavior.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation (optional)
   */
  public $description = '';

  public $target_term_prefix = '';

  /**
   * Whether this filter is enabled or disabled by default.
   *
   * @var bool (optional)
   */
  public $status = TRUE;
  
  /**
   * The class name for editing toolbar tab - is empty for id='none'.
   *
   * @var string 
   */
  public $tbtab_edit_class = FALSE;
  
  public $enforce_enabled = TRUE;   // do not allow to disable
  public $entity_required = FALSE;
  public $entity_required_hint = '';
  public $entity_is_fixed = FALSE;
  public $lock_entity_required = TRUE;

}
