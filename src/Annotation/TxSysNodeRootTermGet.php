<?php

/**
 * @file
 * Contains \Drupal\slogtx\Annotation\TxSysNodeRootTermGet.
 */

namespace Drupal\slogtx\Annotation;


/**
 * @Annotation
 */
class TxSysNodeRootTermGet extends TxRootTermGetBase {

}
