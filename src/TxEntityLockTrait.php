<?php

/**
 * @file
 * Contains \Drupal\slogtx\TxEntityLockTrait.
 */

namespace Drupal\slogtx;

use Drupal\slogtx\SlogTx;
use Drupal\slogtx\Entity\TxTermBase;

trait TxEntityLockTrait {
  
  /**
   * Whether there is a record for the entity/operation. 
   * 
   * @param string $operation ('delete', 'update', ...)
   * @return boolean
   *  TRUE if a record exists, FALSE otherwise.
   */
  public function hasLockRecord($operation) {
    if ($locked_key = $this->getLockedKey($operation)) {
//todo::cleanup
//      $locked = \Drupal::state()->get($locked_key);
      $locked = SlogTx::getLockedStorage()->get($locked_key);
      return isset($locked[$this->id()]);
    }
    return FALSE;
  }

  /**
   * Determines whether the entity is locked for operation.
   *
   * @param string $operation ('delete', 'update', ...)
   *   Dependent on allowed operations of the entity type.
   *   See "slog_is_lockable" key in entity's annotation, but allowed values 
   *   for "slog_is_lockable" are set in self::getAllAllowedOperations().
   * @return string|false
   *   The module name that locks the entity or FALSE.
   */
  public function isLocked($operation) {
    $is_locked = FALSE;
    if ($locked_key = $this->getLockedKey($operation)) {
//      $locked = \Drupal::state()->get($locked_key);
      $locked = SlogTx::getLockedStorage()->get($locked_key);
      $locked = @$locked[$this->id()];
      if (isset($locked)) {
        if (is_array($locked)) {
          $is_locked = (boolean) $locked['locked'];
        }
        else {
          $is_locked = !empty($locked);
        }
      }
    }

    return $is_locked;
  }

  /**
   * Return, who is locking the operation.
   * 
   * @param string $operation ('delete', 'update', ...)
   * @return string
   */
  public function isLockedBy($operation) {
    if ($locked_key = $this->getLockedKey($operation)) {
//      $locked = \Drupal::state()->get($locked_key);
      $locked = SlogTx::getLockedStorage()->get($locked_key);
      $locked = @$locked[$this->id()];
      if (isset($locked)) {
        if (is_string($locked)) {
          $locked_by = $locked;
        }
        elseif (is_array($locked) && !empty($locked['locked'])) {
          $locked_by = $locked['locked'];
        }
      }
    }
    if (empty($locked_by) || $locked_by === 'null') {
      $locked_by = t('System');
    }

    return $locked_by;
  }

  /**
   * Lock the entity for delete, disable, update, ...
   * 
   * - For entities only.
   * - Keep track of changing locks
   * - Delete lock for $blocker==NULL
   * 
   * @param array|boolean $operations 
   *   TRUE or array with some of 'delete', 'update', ...
   *   If $operations === TRUE, then lock 'delete'
   * @param string|boolean $blocker 
   *   Name of the blocking module.
   *   Boolean from locking manager gets converted to an 
   *   associative array with elements locked, user, date and original.
   *   If NULL, the entry is deleted.
   */
  public function lock($operations, $blocker = 'lock_manager') {
    if ($this->isLockable()) {
      $user = \Drupal::currentUser();
      $date = \Drupal::service('date.formatter')->format(REQUEST_TIME, 'short');
      // prepare blocker
      if (empty($blocker)) {
        $blocker = NULL;
      }
      if (is_bool($blocker)) {
        $blocker = [
          'locked' => $blocker,
          'user' => $user->getAccountName(),
          'date' => $date,
        ];
      }
      elseif ($blocker === 'lock_manager') {
        $blocker = $user->getAccountName() . '-' . $date;
      }

      // prepare operations
      if (is_string($operations)) {
        $operations = [$operations];
      }
      elseif ($operations === true) {
        $operations = ['delete'];
      }

      // execute changes for each operation
      if (is_array($operations)) {
        $entity_id = $this->id();
        $all_allowed = self::getAllAllowedOperations();
        foreach ($operations as $operation) {
          if (in_array($operation, $all_allowed)) {
            $locked_key = $this->getLockedKey($operation);
//            $locked = \Drupal::state()->get($locked_key);
            $locked = SlogTx::getLockedStorage()->get($locked_key);
            $has_data = is_array($locked);
            $locked = is_array($locked) ? $locked : [];
            if (is_array($blocker)) {
              $existing = $locked[$entity_id];
              if (is_array($existing)) {
                $blocker += $existing;
              }
              else {
                $blocker['original'] = $existing;
              }
            }

            if (isset($blocker)) {
              $locked[$entity_id] = $blocker;
            }
            else {
              // delete lock if blocker is not set 
              unset($locked[$entity_id]);
            }

            if (!empty($locked)) {
//              \Drupal::state()->set($locked_key, $locked);
              SlogTx::getLockedStorage()->set($locked_key, $locked);
            }
            elseif ($has_data) {
              // delete record
//              \Drupal::state()->delete($locked_key);
              SlogTx::getLockedStorage()->delete($locked_key);
            }
          }
        }
      }
    }
  }

  /**
   * Returns whether the entity type of the entity is lockable.
   * 
   * @return boolean
   *   TRUE if is lockable at all, i.e. undependent of the operations
   */
  public function isLockable() {
    return SlogTx::isEntityTypeLockable($this->getEntityTypeId());
  }

  /**
   * Return an array with allowed operations.
   * 
   * Allowed are:
   *  - delete: deletion is not allowed, neither for admin 
   *  - update: editing is not allowed$entity_id
   *  - disable: disabling is not allowed (root term only)  
   *  - preserve: prevents unlock "delete" for root terms after delition 
   *    of the last child term. This is for required required root terms,
   *    e.g. 'Default', 'Trash', ...
   * 
   * @return array 
   */
  public static function getAllAllowedOperations() {
    return ['delete', 'update', 'disable', 'preserve', 'reorder'];
  }

  /**
   * Return the lock key for locking storage.
   * 
   * - There are two different key: 
   *   1. for toolbars and vocabularies
   *   2. for terms (target terms and root terms)
   * - Locks are stored as state in key_value table.
   * - Each operation there has its own key. 
   * 
   * @param string $operation ('delete', 'update', ...)
   * @return string 
   *   The locked key (name field in table key_value)
   */
  protected function getLockedKey($operation) {
    if ($this instanceof TxTermBase) {
      return $this->getTermLockedKey($operation);
    }
    elseif ($this->isLockable()) {
      return "slogtx.locked.tx.{$operation}";
    }

    return FALSE;
  }

}
