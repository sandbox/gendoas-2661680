<?php
/**
 * @file
 * Contains \Drupal\slogtx\SelfTest.
 */

namespace Drupal\slogtx;

use Drupal\myphpunit\MyPhpUnitTestCase;

/**
 * For testing module myphpunit tests
 *
 * @group slogtx
 */
class SelfTest extends MyPhpUnitTestCase {
  // erst mal keine tests, 
  // da selbstgestricktes nicht für die Veröffentlichung taugt.

  public function testFailure() {
    $this->assertTrue(TRUE, 'SelfTest: expected Failure.');
  }
  public function testSuccess() {
    $this->assertTrue(true, 'SelfTest: expected no Failure.');
  }

}
